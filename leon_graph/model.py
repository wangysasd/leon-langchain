import openai
from langchain_community.chat_models import ChatOpenAI

from config.config import CONFIG

llm_gpt4o = ChatOpenAI(api_key=CONFIG.OPENAI_API_KEY, base_url=CONFIG.OPENAI_BASE_URL,
                       model_name="gpt-4o", temperature=CONFIG.TEMPERATURE)

llm_gpt3 = ChatOpenAI(api_key=CONFIG.OPENAI_API_KEY, base_url=CONFIG.OPENAI_BASE_URL,
                      model_name="gpt-3.5-turbo", temperature=CONFIG.TEMPERATURE)

client = openai.OpenAI(
    api_key=CONFIG.OPENAI_API_KEY, base_url=CONFIG.OPENAI_BASE_URL
)
default_model = "gpt-3.5-turbo"


def get_completion(
        prompt: str,
        system_message: str = "You are a helpful assistant.",
        model: str = default_model,
        temperature: float = 0.3,
        json_mode: bool = False,
):
    response = client.chat.completions.create(
        model=model,
        temperature=temperature,
        top_p=1,
        messages=[
            {"role": "system", "content": system_message},
            {"role": "user", "content": prompt},
        ],
    )
    return response.choices[0].message.content


if __name__ == '__main__':
    print(get_completion("你好"))
