from langchain.tools import Tool
from llc.model_io.models import search
from langchain import hub
from llc.model_io.models import llm
from langchain.agents import AgentExecutor, create_self_ask_with_search_agent


prompt = hub.pull("hwchase17/self-ask-with-search")

tools = [
    Tool(
        name="Intermediate Answer",
        func=search.run,
        description="useful for when you need to ask with search.",
    )
]

if __name__ == '__main__':
    # self_ask_with_search_agent 只能传一个名为 'Intermediate Answer' 的 tool
    agent = create_self_ask_with_search_agent(llm, tools, prompt)
    agent_executor = AgentExecutor(agent=agent, tools=tools, verbose=True)

    response = agent_executor.invoke({"input": "周杰伦老婆叫什么"})
    print('111111111111111')