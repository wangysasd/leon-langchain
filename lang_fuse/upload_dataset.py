import json
from langfuse import Langfuse
from langfuse.model import CreateDatasetRequest, CreateDatasetItemRequest
from tqdm import tqdm
from config.config import CONFIG

data = []



langfuse = Langfuse()

if __name__ == '__main__':
    # langfuse.create_dataset(name="assistant-data")

    with open("my_annotations.jsonl", 'r', encoding='utf-8') as fp:
        for line in fp:
            example = json.loads(line.strip())
            item = {
                "input": {
                    "outlines": example["outlines"],
                    "user_input": example["user_input"]
                },
                "expected_output": example["label"]
            }
            data.append(item)

    for item in tqdm(data[:5]):
        langfuse.create_dataset_item(
            dataset_name="assistant-data",
            input=item["input"],
            expected_output=item["expected_output"]
        )