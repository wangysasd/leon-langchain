from sentence_transformers import CrossEncoder
from sentence_transformers import SentenceTransformer
from native.rag.embedding_util import cos_sim, l2

# 数据
query = "国际争端"
documents = [
    "联合国就苏丹达尔富尔地区大规模暴力事件发出警告",
    "土耳其、芬兰、瑞典与北约代表将继续就瑞典“入约”问题进行谈判",
    "日本岐阜市陆上自卫队射击场内发生枪击事件 3人受伤",
    "国家游泳中心（水立方）：恢复游泳、嬉水乐园等水上项目运营",
    "我国首次在空间站开展舱外辐射生物学暴露实验",
]

# # 模型一: 向量化字段
# model_bge = SentenceTransformer(r'C:\emodel\bge-large-zh-v1.5')
# query_vec = model_bge.encode(query, normalize_embeddings=True)
# doc_vecs = [
#     model_bge.encode(doc, normalize_embeddings=True)
#     for doc in documents
# ]
#
# print("Cosine distance:")  # 越大越相似
# print(cos_sim(query_vec, query_vec))
# for vec in doc_vecs:
#     print(cos_sim(query_vec, vec))

# 模型二: 交叉相关性
model_ce = CrossEncoder(r'C:\emodel\ms-marco-MiniLM-L-6-v2', max_length=512)
scores = model_ce.predict([[query, doc] for doc in documents])

# 按得分排序
sorted_list = sorted(zip(scores, documents), key=lambda x: x[0], reverse=True)
for score, doc in sorted_list:
    print(f"{score}\t{doc}\n")
