from langchain_community.document_loaders import YoutubeLoader
from langchain_community.vectorstores import Chroma
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.chains import ChatVectorDBChain, ConversationalRetrievalChain
from langchain.prompts.chat import (
    ChatPromptTemplate,
    SystemMessagePromptTemplate,
    HumanMessagePromptTemplate
)
from llc.model_io.models import llm, embeddings

# load
loader = YoutubeLoader.from_youtube_url('https://www.youtube.com/watch?v=Dj60HHy-Kqk')
documents = loader.load()

# 分片
text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=20)
split_documents = text_splitter.split_documents(documents)

# 存储并构建检索器
vector_store = Chroma.from_documents(documents, embeddings)
retriever = vector_store.as_retriever()

system_template = """
Use the following context to answer the user's question.
If you don't know the answer, say you don't, don't try to make it up. And answer in Chinese.
-----------
{context}
-----------
{chat_history}
"""

messages = [
    SystemMessagePromptTemplate.from_template(system_template),
    HumanMessagePromptTemplate.from_template('{question}')
]

prompt = ChatPromptTemplate.from_messages(messages)

qa = ConversationalRetrievalChain.from_llm(llm=llm, retriever=retriever, condense_question_prompt=prompt, verbose=True)
chat_history = []

if __name__ == '__main__':
    while True:
        question = input('问题:')
        if question == 'q':
            break
        result = qa({'question': question, 'chat_history': chat_history})
        chat_history.append((question, result['answer']))
        print(result['answer'])
