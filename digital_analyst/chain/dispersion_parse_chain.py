from langchain_core.output_parsers import StrOutputParser, PydanticOutputParser
from langchain_core.prompts import PromptTemplate

from digital_analyst.llm.llm import llm_gpt4, llm_gpt3
from digital_analyst.models.data_model import *

llm = llm_gpt4

parser = PydanticOutputParser(pydantic_object=ConclusionResult)

template = '''
        ##
        你的任务是判断一家公司是否股东分散。
        
        判断规则为：
        如果第一大股东持股比例小于10%，或者前十大股东持股比例低于40%，那么就认定其股东分散。
        
        ##
        你可以基于一下信息进行判断：
        {qa}
        
        ##
        {format_instructions}
        
        ##
        请用中文回答：
        是否股东分散并说明原因。"
    '''

prompt = PromptTemplate(
    template=template,
    input_variables=["qa"],
    partial_variables={"format_instructions": parser.get_format_instructions()},
)

chain = prompt | llm | parser

if __name__ == '__main__':
    QA = """
        Q:中原银行的第一大股东是谁。
        A:中原银行的第一大股东是河南投资集团有限公司。
    
        Q:中原银行第一大股东的持股比例是多少。
        A:河南投资集团有限公司为中原银行第一大股东，持股比例为6.20%。
    
        Q:中原银行的前十大股东持股比例之和是多少。
        A:23.97%
    """

    output = chain.invoke({"qa": QA})

    print(output)
