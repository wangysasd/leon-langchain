# from langchain_community.document_loaders import DirectoryLoader
# from langchain.chains import RetrievalQA
from langchain_community.vectorstores import Chroma
from langchain.text_splitter import CharacterTextSplitter
from llc.model_io.models import llm
from llc.model_io.models import embeddings
from langchain_community.document_loaders import PyPDFLoader
from langchain import VectorDBQA

# loader = DirectoryLoader('./data/', glob='**/*.pdf')
# documents = loader.load()

loader = PyPDFLoader(r".\data\llama2.pdf")
documents = loader.load_and_split()

text_splitter = CharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
splits_docs = text_splitter.split_documents(documents)

docsearch = Chroma.from_documents(splits_docs, embeddings)
retriever = docsearch.as_retriever(search_kwargs={'k': 1})

qa = VectorDBQA.from_chain_type(llm=llm, chain_type='stuff', vectorstore=docsearch, return_source_documents=True)

if __name__ == '__main__':
    result = qa({"query": "llama 2有多少参数？"})
    print(result['result'])
