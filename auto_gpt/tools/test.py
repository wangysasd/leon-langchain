import pandas as pd
# Load the data from the Excel file
file_path = '../data/2023年8月-9月销售记录.xlsx'
data = pd.read_excel(file_path)
# Sum up the '销售量' column
total_sales = data['销售量'].sum()
# Print the result
print("总销售量为:", total_sales)