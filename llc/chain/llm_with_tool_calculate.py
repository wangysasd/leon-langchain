from langchain_core.tools import tool
from langchain_core.output_parsers import StrOutputParser
from langchain.output_parsers import JsonOutputToolsParser
from llc.model_io.models import llm


@tool
def multiply(first_int: int, second_int: int) -> int:
    """两个整数相乘"""
    return first_int * second_int


@tool
def add(first_int: int, second_int: int) -> int:
    """Add two integers."""
    return first_int + second_int


@tool
def exponent(base: int, exponent: int) -> int:
    """Exponent the base to the exponent power"""
    return base ** exponent


# Demo 1:
tools = [multiply, add, exponent]
llm_with_tools = llm.bind_tools(tools) | {
    "functions": JsonOutputToolsParser(),
    "text": StrOutputParser()
}


if __name__ == '__main__':
    print(llm_with_tools.invoke("1024的16倍是多少"))
