from config.config import CONFIG
from langchain_openai import ChatOpenAI
from langchain_openai import OpenAIEmbeddings
from langchain_community.utilities import SerpAPIWrapper
# from langchain_community.utilities.serpapi import SerpAPIWrapper

llm = ChatOpenAI(api_key=CONFIG.OPENAI_API_KEY, base_url=CONFIG.OPENAI_BASE_URL,
                 model_name=CONFIG.MODEL_NAME, temperature=CONFIG.TEMPERATURE)

llm_gpt4 = ChatOpenAI(api_key=CONFIG.OPENAI_API_KEY, base_url=CONFIG.OPENAI_BASE_URL,
                      model_name="gpt-4-1106-preview", temperature=CONFIG.TEMPERATURE)

embeddings = OpenAIEmbeddings(openai_api_key=CONFIG.OPENAI_API_KEY, openai_proxy=CONFIG.OPENAI_BASE_URL)

search = SerpAPIWrapper(serpapi_api_key=CONFIG.SERPAPI_API_KEY)
