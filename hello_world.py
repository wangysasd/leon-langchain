from langchain_openai import ChatOpenAI
from openai import OpenAI
from config.config import CONFIG

client = OpenAI(api_key=CONFIG.OPENAI_API_KEY, base_url=CONFIG.OPENAI_BASE_URL)

# gpt-3.5-turbo-0125
def get_completion(prompt, model="gpt-3.5-turbo"):
    messages = [{"role": "user", "content": prompt}]
    stream = client.chat.completions.create(
        model=model,
        messages=messages,
        temperature=0
    )
    return stream.choices[0].message.content


x = ChatOpenAI()

if __name__ == '__main__':
    # print(get_completion("你好呀"))
    print(x.invoke("11111"))