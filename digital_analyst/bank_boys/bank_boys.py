from langchain.agents import AgentExecutor

from digital_analyst.chain.basic_qa_chain import runnable_chain as qa_chain
from digital_analyst.agent.research_agent import agent, tools
from digital_analyst.agent.rp_agent import rp_agent, rp_tools
from digital_analyst.agent.rep_agent import rep_agent, rep_tools
from digital_analyst.agent.dispersion_agent import dispersion_prompt
from digital_analyst.agent.background_agent import background_prompt
from digital_analyst.tools.python_excel_tool import ExcelAnalyser
from digital_analyst.util.fileutil import get_report_summary

qa_str = ""
chat_history = []

excel_analyser = ExcelAnalyser()


def basic_qa(q, prompt=None):
    global qa_str
    if prompt is None:
        prompt = q
    a = qa_chain.invoke(prompt)
    qa_str += "Q:" + q + "\n"
    qa_str += "A:" + a + "\n"
    qa_str += "\n"


if __name__ == '__main__':
    # 逐个回答问题
    qa_str += "1.公司治理：\n"
    qa_str += "1.1成立背景：\n"
    qa_str += "\n"

    basic_qa("中原银行是哪年成立的")
    basic_qa("中原银行的银行性质是什么")
    basic_qa("中原银行的注册资本有多少")

    qa_str += "1.2股东情况：\n"
    basic_qa("中原银行的大股东是谁。")

    q = "中原银行是否有股东是否分散。"
    agent_executor_dispersion = AgentExecutor(agent=agent, tools=tools, handle_parsing_errors=True, verbose=True)
    a = agent_executor_dispersion.invoke({"input": dispersion_prompt, "chat_history": chat_history})['output']
    qa_str += "Q:" + q + "\n"
    qa_str += "A:" + a + "\n"
    qa_str += "\n"

    q = "中原银行是民营背景强还是国企背景强。"
    agent_executor_background = AgentExecutor(agent=agent, tools=tools, handle_parsing_errors=True, verbose=True)
    a = agent_executor_background.invoke({"input": background_prompt, "chat_history": chat_history})['output']
    qa_str += "Q:" + q + "\n"
    qa_str += "A:" + a + "\n"
    qa_str += "\n"

    qa_str += "1.3高管变动：\n"
    basic_qa("请列出中原银行的高管名单, 以及高管名单的近期的变动。")

    qa_str += "2.经营情况：\n"
    qa_str += "2.1区域经济：\n"
    basic_qa("请对中原银行所处地区的经济状况和政府支持情况进行描述，用数字说话。最后不要给结论或评价。")

    qa_str += "2.2资产端：\n"
    q = "中原银行股份有限公司的总资产是多少，其总资产在行业的分位数是多少（保留两位小数，单位是%）。"
    a = excel_analyser.analyse(q)
    qa_str += "Q:" + q + "\n"
    qa_str += "A:" + a + "\n"
    qa_str += "\n"

    basic_qa("请简述中原银行的个人贷款和对公贷款情况，尽可能用数字说话。")
    basic_qa("中原银行的贷款情况可以从区域、行业以及个人和对公两个维度进行分析。")
    basic_qa("中原银行的金融投资规模多少，近些年如何变化。")
    basic_qa("中原银行的金融投资规模结构如何。")

    qa_str += "2.3负债端：\n"
    basic_qa("请描述中原银行的存款规模、增速情况。可以区域和个人对公两个维度进行分析。")

    q = "中原银行的同业负债是规模和占比情况，如果同业负债占比超过20%，要点明其仰赖同业负债。"
    rp_agent_executor = AgentExecutor(agent=rp_agent, tools=rp_tools, handle_parsing_errors=True, verbose=True)
    a = rp_agent_executor.invoke({
                                     "input": "中原银行的同业负债是规模和占比情况，如果同业负债占比超过20%（请用PythonTool来做判断），要点明其仰赖同业负债。",
                                     "chat_history": chat_history})['output']
    qa_str += "Q:" + q + "\n"
    qa_str += "A:" + a + "\n"
    qa_str += "\n"

    qa_str += "3.财务风险：\n"
    qa_str += "3.1资产质量：\n"
    basic_qa("中原银行的资产质量如何，请用数据回答")
    qa_str += "3.2盈利能力：\n"
    basic_qa("中原银行的盈利能力如何，有何财务风险，请用数据回答。")

    q = "中原银行的净利润和净息差为多少，城市商业银行的平均值相比如何。（你可以调用PythonTool来比较两个数字的大小，不要自己去比）"
    rep_agent_executor = AgentExecutor(agent=rep_agent, tools=rep_tools, handle_parsing_errors=True, verbose=True)
    a = rep_agent_executor.invoke({"input": q, "chat_history": chat_history})['output']
    qa_str += "Q:" + q + "\n"
    qa_str += "A:" + a + "\n"
    qa_str += "\n"

    qa_str += "3.3资本情况：\n"
    basic_qa("中原银行的资本充足率、一级资本充足率和核心一级资本充足率多少，对其资本充足情况给与评价。")

    print(qa_str)
    with open("../data/qa.txt", "a") as f:
        f.write(qa_str)

    # 总结报告
    final_report = get_report_summary()
    print(final_report)
    with open("../data/final_report.txt", "w") as f:
        f.write(final_report)
