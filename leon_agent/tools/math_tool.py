from langchain.chains.llm_math.base import LLMMathChain

from leon_agent.llm import llm

compute = LLMMathChain.from_llm(llm=llm)


def math_func(input: str):
    compute.invoke(input)


if __name__ == '__main__':
    input = "河南投资集团有限公司为中原银行第一大股东，持股比例为6.2%，请问其持股比例是否超过10%"
    print(math_func(input))
