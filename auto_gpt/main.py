from auto_gpt.agent.auto_gpt import AutoGPT
from llm.model import llm, embeddings
from langchain_community.vectorstores import Chroma
from langchain.schema import Document
from auto_gpt.tools import *
from auto_gpt.tools.python_tool import ExcelAnalyser

MY_VERBOSE = True


def launch_agent(agent: AutoGPT):
    human_icon = "\U0001F468"
    ai_icon = "\U0001F916"

    print(f"{ai_icon}: "+"您好，我是数字研究员PAI。我会读PDF文件，我会用EXCEL分析数据，我还会写PYTHON代码哦。")

    while True:
        task = input(f"{ai_icon}: 有什么可以帮您？ \n{human_icon}: ")
        if task.strip().lower() == "quit":
            break
        reply = agent.run(task, verbose=True, my_verbose=MY_VERBOSE)
        print(f"{ai_icon}: {reply}\n")


def main():
    # 存储长时记忆的向量数据库
    db = Chroma.from_documents([Document(page_content="")], embeddings)
    retriever = db.as_retriever(
        search_kwargs={"k": 1}
    )

    tools = [
        document_qa_tool,
        document_generation_tool,
        email_tool,
        excel_inspection_tool,
        directory_inspection_tool,
        finish_placeholder,
        ExcelAnalyser(
            prompt_file="./prompts/tools/excel_analyser.txt",
            verbose=True,
            my_verbose=MY_VERBOSE).as_tool()
    ]

    agent: AutoGPT = AutoGPT(
        llm=llm,
        tools=tools,
        work_dir="./data",
        main_prompt_file="./prompts/main/main.txt",
        final_prompt_file="./prompts/main/final_step.txt",
        memory_retriever=retriever
    )

    launch_agent(agent)


if __name__ == '__main__':
    main()


