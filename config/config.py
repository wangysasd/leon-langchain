import os
from dotenv import load_dotenv
import logging

logger = logging.Logger(__name__)
load_dotenv()

default_env_values = {
    "MODE": "PROD",
    "PURPOSE": "API",
    "MODEL_NAME": 'gpt-3.5-turbo',
    "TEMPERATURE": 0,
    "SERPAPI_API_KEY": "8e7690a09e8fa5b000d3393c3553e143829c02021654c703444e9c0f5a6c6bd0"
}


def load_str_env(name: str, required: bool = False) -> str:
    """
    Load environment variable as string
    :param name: name of the environment variable
    :param required: whether the environment variable is required
    """
    if os.environ.get(name):
        return os.environ.get(name)

    if default_env_values.get(name) is not None:
        return default_env_values.get(name)

    if required:
        raise Exception(f"Env {name} is not set")


def load_int_env(name: str, required: bool = False) -> int:
    """
    Load environment variable as int
    :param name: name of the environment variable
    :param required: whether the environment variable is required
    """
    if os.environ.get(name):
        return int(os.environ.get(name))

    if default_env_values.get(name) is not None:
        return int(default_env_values.get(name))

    if required:
        raise Exception(f"Env {name} is not set")


class Config:
    """Backend configuration"""

    def __init__(self):
        from app import __VERSION__

        logger.info(f"Init Config")

        self.OPENAI_API_KEY = load_str_env('OPENAI_API_KEY', required=True)
        self.OPENAI_BASE_URL = load_str_env('OPENAI_BASE_URL', required=True)
        self.MODEL_NAME = load_str_env("MODEL_NAME", required=False)
        self.TEMPERATURE = load_int_env("TEMPERATURE", required=False)
        self.SERPAPI_API_KEY = load_str_env("SERPAPI_API_KEY", required=True)
        self.LANGFUSE_SECRET_KEY = load_str_env("LANGFUSE_SECRET_KEY", required=True)
        self.LANGFUSE_PUBLIC_KEY = load_str_env("LANGFUSE_PUBLIC_KEY", required=True)
        self.LANGFUSE_HOST = load_str_env("LANGFUSE_HOST", required=True)

        # version
        self.VERSION = __VERSION__


CONFIG = Config()
