import chromadb
from chromadb.config import Settings
from native.rag.embedding_util import get_embeddings
from native.rag.pdf_utils import extract_text_from_pdf, split_text


class MyVectorDBConnector:
    def __init__(self, collection_name, embedding_fn):
        chromadb_client = chromadb.Client(Settings(allow_reset=True))
        # 为了演示，实际不需要每次reset。
        # chromadb_client.reset()
        # 创建一个collection
        self.collection = chromadb_client.get_or_create_collection(
            name=collection_name
        )
        self.embedding_fn = embedding_fn

    def add_documents(self, documents):
        """向colection中添加文档和向量"""
        self.collection.add(
            embeddings=self.embedding_fn(documents),  # 每个文档的向量
            documents=documents,
            ids=[f"id{i}" for i in range(len(documents))]  # 每个文档的id
        )

    def search(self, query, top_n):
        """检索向量数据库"""
        results = self.collection.query(
            query_embeddings=self.embedding_fn([query]),
            n_results=top_n
        )
        return results


if __name__ == '__main__':
    # 获取文本
    paragraphs = extract_text_from_pdf("../../static/llama2.pdf", min_line_length=10)
    chunks = split_text(paragraphs)

    # 存入向量数据库
    vector_db = MyVectorDBConnector("demo", get_embeddings)
    vector_db.add_documents(chunks)
    # 查询结果
    user_query = "Llama2有多少参数"
    results = vector_db.search(user_query, 2)
    for para in results['documents'][0]:
        print(para)
    print(1)
