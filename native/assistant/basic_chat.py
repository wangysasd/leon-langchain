from util import *

assistant = client.beta.assistants.create(
    name="AGIClass Demo",
    description="你叫瓜瓜，你是AGI课堂的智能助理。你负责回答与AGI课堂有关的问题。",
    model="gpt-4-turbo",  # gpt-4-1106-preview
)

thread = client.beta.threads.create(
    metadata={"fullname": "王西瓜", "username": "taliux"},
    messages=[
        {"role": "user",
         "content": "你好"},
        {"role": "assistant",
         "content": "有什么可以帮您"},
        {"role": "user",
         "content": "你是谁"}
    ]
)

# 从上下文获取thread
# thread = client.beta.threads.retrieve(thread.id)


if __name__ == '__main__':

    # 通过message的方法来聊天。
    message = client.beta.threads.messages.create(
        thread_id=thread.id,
        role="user",
        content="你都能做什么?",
    )

    print_result(assistant, thread)

    print("=========打印thread看看啥样子=============")
    show_json(thread)

    print("===========看看thread中的message列表==============")
    show_json(client.beta.threads.messages.list(thread.id))


