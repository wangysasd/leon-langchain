from langchain.agents import AgentType, initialize_agent
from langchain.chains.llm_math.base import LLMMathChain
from langchain_core.tools import Tool

from llc.model_io.models import search, llm

llm_math_chain = LLMMathChain(llm=llm, verbose=True)

# 创建一个功能列表，指明这个 agent 里面都有哪些可用工具，agent 执行过程可以看必知概念里的 Agent 那张图
tools = [
    Tool(
        name="Search",
        func=search.run,
        description="useful for when you need to answer questions about current events"
    ),
    Tool(
        name="Calculator",
        func=llm_math_chain.run,
        description="useful for when you need to answer questions about math"
    )
]

# 初始化 agent
agent = initialize_agent(tools, llm, agent=AgentType.ZERO_SHOT_REACT_DESCRIPTION, verbose=True)

if __name__ == '__main__':
    # response = search.run("苹果的市值是多少")
    # print(response)

    # 执行 agent
    agent.invoke(
        {'input':
             "Who is Leo DiCaprio's girlfriend? What is her current age raised to the 0.43 power?"})
