from util import *

vector_store = client.beta.vector_stores.create(
    name="MyVectorStore"
)

file = client.files.create(
    file=open("agiclass_intro.pdf", "rb"),
    purpose="assistants"
)

vector_store_file = client.beta.vector_stores.files.create(
    vector_store_id=vector_store.id,
    file_id=file.id
)

assistant = client.beta.assistants.create(
    instructions="你是个问答机器人，你根据给定的知识回答用户问题。",
    model="gpt-4-turbo",
    tools=[{"type": "file_search"}],
    file_ids=[file.id]
)

assistant = client.beta.assistants.update(
    assistant_id=assistant.id,
    tool_resources={"file_search": {"vector_store_ids": [vector_store.id]}}
)

thread = client.beta.threads.create()

message = client.beta.threads.messages.create(
    thread_id=thread.id,
    role="user",
    content="你都能做什么?",
)

print_result(assistant,thread)