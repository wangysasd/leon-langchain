import json

from openai.lib.streaming import AssistantEventHandler
from typing_extensions import override

from native.llm import client


def show_json(obj):
    """把任意对象用排版美观的JSON格式打印出来"""
    print(json.dumps(
        json.loads(obj.model_dump_json()),
        indent=4,
        ensure_ascii=False
    ))


class EventHandler(AssistantEventHandler):
    @override
    def on_text_created(self, text) -> None:
        """响应回复创建事件"""
        print(f"\nassistant > ", end="", flush=True)

    @override
    def on_text_delta(self, delta, snapshot):
        """响应输出生成的流片段"""
        print(delta.value, end="", flush=True)

    @override
    def on_tool_call_created(self, tool_call):
        """响应工具调用"""
        print(f"\nassistant > {tool_call.type}\n", flush=True)

    @override
    def on_tool_call_delta(self, delta, snapshot):
        """响应工具调用的流片段"""
        if delta.type == 'code_interpreter':
            if delta.code_interpreter.input:
                print(delta.code_interpreter.input, end="", flush=True)
        if delta.code_interpreter.outputs:
            print(f"\n\noutput >", flush=True)
            for output in delta.code_interpreter.outputs:
                if output.type == "logs":
                    print(f"\n{output.logs}", flush=True)


def print_result(assistant, thread):

    run = client.beta.threads.runs.create_and_poll(
        thread_id=thread.id,
        assistant_id=assistant.id
    )

    if run.status == 'completed':
        messages = client.beta.threads.messages.list(
            thread_id=thread.id
        )
        print(messages.data[0].content[0].text.value)
    else:
        print(run.status)

    # while True:
    #     run = client.beta.threads.runs.retrieve(
    #         thread_id=thread.id,
    #         run_id=run.id
    #     )
    #     if run.status == "completed":
    #         break
    #     time.sleep(1)
    #
    # messages = client.beta.threads.messages.list(
    #     thread_id=thread.id
    # )
    #
    # print(messages.data[0].content[0].text.value)















# message = client.beta.threads.messages.create(
#     thread_id=thread.id,
#     role="user",
#     content="你说什么?"
# )
#
# with client.beta.threads.runs.stream(
#         thread_id=thread.id,
#         assistant_id=assistant.id,
#         event_handler=EventHandler(),
# ) as stream:
#     stream.until_done()
