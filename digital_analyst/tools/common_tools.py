from langchain import hub
from langchain.agents import create_react_agent, AgentExecutor
from langchain_core.tools import Tool, tool

from digital_analyst.llm.llm import llm_gpt4

llm = llm_gpt4


@tool
def multiply(first_int: int, second_int: int) -> int:
    """Multiply two integers together."""
    return first_int * second_int


tools = [
    Tool(
        name="Multiply",
        description="Multiply two integers together.",
        func=multiply,
    )]

prompt = hub.pull("hwchase17/react-chat")
agent = create_react_agent(llm, tools, prompt)
agent_executor = AgentExecutor(agent=agent, tools=tools, handle_parsing_errors=True, verbose=True)

if __name__ == '__main__':
    prompt = "5乘以3等于多少"

    chat_history = []

    response = agent_executor.invoke({"input": prompt, "chat_history": chat_history})['output']

    print(response)
