from enum import Enum


class SolutionType(Enum):
    RAG = 1
    Agent = 2
