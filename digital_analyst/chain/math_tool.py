from langchain.chains.llm_math.base import LLMMathChain

from digital_analyst.llm.llm import llm_gpt4

llm = llm_gpt4
compute = LLMMathChain.from_llm(llm=llm)

if __name__ == '__main__':
    print(compute.invoke("河南投资集团有限公司为中原银行第一大股东，持股比例为6.2%，请问其持股比例是否超过10%"))