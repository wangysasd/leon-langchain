from config.config import CONFIG
from langchain_openai import ChatOpenAI
from langchain_openai import OpenAIEmbeddings
from langchain_community.utilities import SerpAPIWrapper

llm_gpt4 = ChatOpenAI(api_key=CONFIG.OPENAI_API_KEY, base_url=CONFIG.OPENAI_BASE_URL,
                      model_name="gpt-4-1106-preview", temperature=CONFIG.TEMPERATURE)
# model_kwargs = {
#     "seed": 42
# }

llm_gpt4o = ChatOpenAI(api_key=CONFIG.OPENAI_API_KEY, base_url=CONFIG.OPENAI_BASE_URL,
                      model_name="gpt-4o", temperature=CONFIG.TEMPERATURE)

llm_gpt3 = ChatOpenAI(api_key=CONFIG.OPENAI_API_KEY, base_url=CONFIG.OPENAI_BASE_URL,
                      model_name=CONFIG.MODEL_NAME, temperature=CONFIG.TEMPERATURE)

embeddings = OpenAIEmbeddings(openai_api_key=CONFIG.OPENAI_API_KEY, openai_proxy=CONFIG.OPENAI_BASE_URL)

search = SerpAPIWrapper(serpapi_api_key=CONFIG.SERPAPI_API_KEY)
