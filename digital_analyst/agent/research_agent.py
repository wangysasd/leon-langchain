from langchain import hub
from langchain.agents import AgentExecutor, create_react_agent
from langchain.chains.llm_math.base import LLMMathChain
from langchain_core.messages import HumanMessage, AIMessage
from langchain_core.tools import Tool

from digital_analyst.util.fileutil import get_answer_from_doc
from digital_analyst.llm.llm import llm_gpt4, llm_gpt4o

llm = llm_gpt4

# 自定义工具字典
tools = [
    Tool(
        name="Calculator",
        description="Useful for when you need to answer questions about math.",
        func=LLMMathChain.from_llm(llm=llm).invoke,
        coroutine=LLMMathChain.from_llm(llm=llm).ainvoke,
    ),

    Tool(
        name="CompanyInfo",
        description="当查询中原银行数据和信息的时候，调用这个方法",
        func=get_answer_from_doc,
    )
]

prompt = hub.pull("hwchase17/react-chat")
agent = create_react_agent(llm, tools, prompt)
agent_executor = AgentExecutor(agent=agent, tools=tools, handle_parsing_errors=True, verbose=True)

# agent = initialize_agent(tools, llm, agent=AgentType.ZERO_SHOT_REACT_DESCRIPTION, verbose=True)

if __name__ == '__main__':

    chat_history = []

    while True:
        question = input("请提问：")
        response = agent_executor.invoke({"input": question, "chat_history": chat_history})['output']
        print(response)
        chat_history.extend(HumanMessage(content=question))
        chat_history.extend(AIMessage(content=response))

