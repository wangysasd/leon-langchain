from langchain.agents import create_react_agent
from langchain_core.prompts import PromptTemplate

from leon_agent.llm import llm
from leon_agent.playground.boot import run_agent
from leon_agent.parser.my_agent_output_parser import MyAgentOutputParser
from leon_agent.tools import tools


def main():
    parser = MyAgentOutputParser()
    prompt = PromptTemplate.from_file("../prompts/main/main.txt").partial(
        work_dir="../data", format_instructions=parser.get_format_instructions())

    agent = create_react_agent(
        llm=llm,
        tools=tools,
        prompt=prompt,
        output_parser=parser
    )

    run_agent(agent, tools)


if __name__ == '__main__':
    main()
