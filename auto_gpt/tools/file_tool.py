import os


def list_files_in_directory(path: str) -> str:
    """list all file names in the directory"""
    file_names = os.listdir(path)

    # join the file names into a sing string, separated by a new line
    return "\n".join(file_names)
