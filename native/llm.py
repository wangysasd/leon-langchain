from openai import OpenAI
from config.config import CONFIG

client = OpenAI(api_key=CONFIG.OPENAI_API_KEY, base_url=CONFIG.OPENAI_BASE_URL)


def get_completion(prompt, model="gpt-3.5-turbo-0125"):
    messages = [{"role": "user", "content": prompt}]
    stream = client.chat.completions.create(
        model=model,
        messages=messages,
        temperature=0
    )
    return stream.choices[0].message.content