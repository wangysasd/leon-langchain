import requests
import json

if __name__ == '__main__':
    response = requests.post(
        "http://localhost:9999/joke/invoke",
        json={'input': {'topic': '小明'}}
    )

    response_json = json.loads(response.text)
    print(response_json['output']['content'])
