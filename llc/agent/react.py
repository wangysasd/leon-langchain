from langchain.tools import Tool, tool
import calendar
import dateutil.parser as parser
from llc.model_io.models import search
from langchain import hub
from llc.model_io.models import llm
from langchain.agents import AgentExecutor, create_react_agent

tools = [Tool.from_function(func=search.run,
                            name="Search",
                            description="useful for when you need to answer question about current events"), ]


@tool("weekday")
def weekday(date_str: str) -> str:
    """Convert date to weekday name"""
    d = parser.parse(date_str)
    return calendar.day_name[d.weekday()]


tools += [weekday]

if __name__ == '__main__':

    # Demo 1:
    prompt = hub.pull("hwchase17/react")
    agent = create_react_agent(llm, tools, prompt)
    agent_executor = AgentExecutor(agent=agent, tools=tools, verbose=False)
    response = agent_executor.invoke({"input": "周杰伦生日那天是星期几"})
    print(response)
