from typing import List
from langchain.schema import Document

from langchain_community.document_loaders import PyPDFLoader
from langchain_community.document_loaders import UnstructuredWordDocumentLoader
from langchain_core.prompts import PromptTemplate
from langchain_core.output_parsers import StrOutputParser

from digital_analyst.llm.llm import *
from digital_analyst.util.CallbackHandlers import ColoredPrintHandler
from digital_analyst.util.PrintUtils import THOUGHT_COLOR

document_cache = {}

llm = llm_gpt4


class FileLoadFactory:
    @staticmethod
    def get_loader(filename: str):
        ext = get_file_extension(filename)
        if ext == "pdf":
            return PyPDFLoader(filename)
        elif ext == "docx" or ext == "doc":
            return UnstructuredWordDocumentLoader(filename)
        else:
            raise NotImplementedError(f"File extension {ext} not supported")


def get_file_extension(filename: str) -> str:
    return filename.split(".")[-1]


def load_docs(filename: str) -> List[Document]:
    file_loader = FileLoadFactory.get_loader(filename)
    pages = file_loader.load_and_split()
    return pages


def get_document(filename: str) -> str:
    document = document_cache.get(filename)

    if document is None:
        document = ""
        raw_docs = load_docs(filename)
        if len(raw_docs) == 0:
            return "抱歉，文档为空"
        for doc in raw_docs:
            document += str(doc.page_content)
        return document


def ask_full_document(filename: str, query: str, ) -> str:
    """根据一个PDF文档的内容，回答一些问题"""

    document = document_cache.get(filename)

    if document is None:
        document = ""
        raw_docs = load_docs(filename)
        if len(raw_docs) == 0:
            return "抱歉，文档为空"
        for doc in raw_docs:
            document += str(doc.page_content)

    prompt = PromptTemplate.from_file("../prompt/tools/document_qa.txt")
    chain = prompt | llm | StrOutputParser()

    response = chain.invoke({
        "query": query,
        "document": document,
    })

    # response = ""
    # print(query)
    # for c in chain.stream({
    #     "query": query,
    #     "document": document,
    # },
    #         config={
    #             "callbacks": [ColoredPrintHandler(THOUGHT_COLOR)]
    #         }
    # ):
    #     response += c
    print("-------------------")
    return response


def get_report_summary():
    prompt = PromptTemplate.from_file("../prompt/tools/report_summary.txt")
    chain = prompt | llm | StrOutputParser()

    qa = PromptTemplate.from_file("../data/qa.txt")

    response = ""
    for c in chain.stream({
        "qa": qa
    },
            config={
                "callbacks": [ColoredPrintHandler(THOUGHT_COLOR)]
            }
    ):
        response += c
    return response


def get_answer_from_doc(query):
    filename = "../data/中原银行股份有限公司2023年跟踪评级报告.pdf"
    each_result_str = ""
    answer = ask_full_document(filename, query)
    each_result_str += "Q:" + query + "\n"
    each_result_str += "A:" + answer + "\n"
    each_result_str += "\n"
    return each_result_str


if __name__ == '__main__':
    # while True:
    #     query = input("请回答")
    #     result = get_answer_from_doc(query)
    #     print(result)

    # 逐个回答问题
    qa_str = ""
    # qa_str += "1.公司治理：\n"
    # qa_str += "1.1成立背景：\n"
    qa_str += get_answer_from_doc("中原银行是哪年成立的")
    # qa_str += get_answer_from_doc("中原银行的银行性质是什么")
    # qa_str += get_answer_from_doc("中原银行的注册资本有多少")
    # qa_str += "1.2股东情况：\n"
    # qa_str += get_answer_from_doc("中原银行的大股东是谁。")
    # qa_str += get_answer_from_doc("中原银行有哪些股东及占比，这些股东都是什么性质的。其中大多数股东为国企还是民企。")
    # qa_str += "1.3高管变动：\n"
    # qa_str += get_answer_from_doc("请列出中原银行的高管名单, 以及高管名单的近期的变动。")
    #
    # qa_str += "2.经营情况：\n"
    # qa_str += "2.1区域经济：\n"
    # qa_str += get_answer_from_doc("请对中原银行所处地区的经济状况进行描述，用数字说话。")
    # qa_str += get_answer_from_doc("中原银行的银行性质是什么")
    # qa_str += "2.2资产端：\n"
    # qa_str += get_answer_from_doc("中原银行的贷款情况可以从区域、行业以及个人和对公两个维度进行分析")
    # qa_str += get_answer_from_doc("中原银行的金融投资规模多少，近些年如何变化。")
    # qa_str += get_answer_from_doc("中原银行的金融投资规模结构如何。")
    # qa_str += "2.3负债端：\n"
    # qa_str += get_answer_from_doc("请描述中原银行的存款规模、增速情况。可以区域和个人对公两个维度进行分析。")
    # qa_str += get_answer_from_doc("请描述中原银行同业负债的规模和占比情况。")
    #
    # qa_str += "3.财务风险：\n"
    # qa_str += "3.1资产质量：\n"
    # qa_str += get_answer_from_doc("中原银行的资产质量如何，请用数据回答")
    # qa_str += "3.2盈利能力：\n"
    # qa_str += get_answer_from_doc("中原银行的盈利能力如何，有何财务风险，请用数据回答。")
    # qa_str += "3.3资本情况：\n"
    # qa_str += get_answer_from_doc("中原银行的资本充足率、一级资本充足率和核心一级资本充足率多少，对其资本充足情况给与评价。")
    #
    # print(qa_str)
    # with open("../data/qa_old.txt", "w") as f:
    #     f.write(qa_str)
    #
    # # 总结报告
    # final_report = get_report_summary()
    # print(final_report)
    # with open("../data/final_report.txt", "w") as f:
    #     f.write(final_report)
