from langfuse import Langfuse
from config.config import CONFIG
from langfuse.callback import CallbackHandler

langfuse = Langfuse(secret_key=CONFIG.LANGFUSE_SECRET_KEY,
                    public_key=CONFIG.LANGFUSE_PUBLIC_KEY,
                    host=CONFIG.LANGFUSE_HOST)
# 按名称加载
# prompt = langfuse.get_prompt("leon-prompt")

# 按名称和版本号加载
prompt = langfuse.get_prompt("leon-prompt", version=3)

# 对模板中的变量赋值
compiled_prompt = prompt.compile(story="老师")

print(compiled_prompt)

