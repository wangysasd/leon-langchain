from langchain import hub
from langchain.agents import AgentExecutor, create_react_agent
from langchain.chains.llm_math.base import LLMMathChain
from langchain_core.messages import HumanMessage, AIMessage
from langchain_core.tools import Tool

from digital_analyst.tools.python_excel_tool import ExcelAnalyser
from digital_analyst.tools.python_tool import python_tool
from digital_analyst.util.fileutil import get_answer_from_doc
from digital_analyst.llm.llm import llm_gpt4

llm = llm_gpt4

# 自定义工具字典
rep_tools = [
    Tool(
        name="PythonTool",
        description="当你需要数学运算、逻辑运算判断，比较两个数字大小的时候，调用这个工具",
        func=python_tool
    ),

    Tool(
        name="ExcelAnalyser",
        description="当你要查询不同类型银行的指标平均值时，调用这个工具",
        func=ExcelAnalyser(filename="../data/金融总局.xlsx").analyse
    ),

    Tool(
        name="CompanyInfo",
        description="当查询中原银行数据和信息的时候，调用这个方法",
        func=get_answer_from_doc,
    )
]

prompt = hub.pull("hwchase17/react-chat")
rep_agent = create_react_agent(llm, rep_tools, prompt)
rep_agent_executor = AgentExecutor(agent=rep_agent, tools=rep_tools, handle_parsing_errors=True, verbose=True)

if __name__ == '__main__':
    question = "中原银行的净利润和净息差为多少，城市商业银行的平均值相比如何。"
    print(question)
    chat_history = []
    response = rep_agent_executor.invoke({"input": question, "chat_history": chat_history})['output']
    print(response)
