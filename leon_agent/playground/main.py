from leon_agent.agent.auto_gpt import AutoGPT

from leon_agent.llm import llm
from leon_agent.playground.boot import launch_agent
from leon_agent.tools import tools


def main():
    # 定义智能体
    agent = AutoGPT(
        llm=llm,
        tools=tools,
        work_dir="../data",
        main_prompt_file="../prompts/main/main.txt",
        max_thought_steps=20,
    )

    # 运行智能体
    launch_agent(agent)


if __name__ == "__main__":
    print("----------------------------")
    main()
