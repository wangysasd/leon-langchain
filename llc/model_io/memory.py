from langchain.memory import ConversationBufferMemory, ConversationBufferWindowMemory, ConversationTokenBufferMemory
from models import llm

# Demo 1: 一直加
history = ConversationBufferMemory()
history.save_context({"input": "111"}, {"output": "222"})
history.save_context({"input": "333"}, {"output": "444"})

# Demo 2: 计数
window = ConversationBufferWindowMemory(k=2)
window.save_context({"input": "111"}, {"output": "222"})
window.save_context({"input": "333"}, {"output": "444"})
window.save_context({"input": "555"}, {"output": "666"})

# Demo 3: 计Token
memory = ConversationTokenBufferMemory(
    llm=llm,
    max_token_limit=40
)
memory.save_context({"input": "111"}, {"output": "222"})
memory.save_context({"input": "333"}, {"output": "444"})

if __name__ == '__main__':
    # print(history.load_memory_variables({}))
    #
    # print(window.load_memory_variables({}))

    print(memory.load_memory_variables({"input": "333"}))
    print(memory.load_memory_variables({}))
