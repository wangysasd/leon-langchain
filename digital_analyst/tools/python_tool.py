import pandas as pd
from langchain_core.output_parsers import StrOutputParser
from langchain_core.output_parsers.openai_tools import JsonOutputKeyToolsParser
from langchain_core.prompts import PromptTemplate
from langchain_experimental.tools import PythonAstREPLTool

from auto_gpt.utils.PrintUtils import CODE_COLOR
from digital_analyst.llm.llm import llm_gpt4
from digital_analyst.util.PrintUtils import color_print

llm = llm_gpt4

tool = PythonAstREPLTool()
parser = JsonOutputKeyToolsParser(key_name=tool.name)
llm_with_tools = llm.bind_tools([tool], tool_choice=tool.name)
code_chain = llm_with_tools | parser | (lambda x: x[0]['query'])

template = """
    ###
    任务：
    你的任务是基于用户的输入回答问题：
    
    ###
    背景：
    对于一些复杂的问题，由于是和数学计算相关的问题。并不容易直接获得答案。
    通常用户将问题转换为一段python代码，通过运行代码来得到问题的答案。
    你自己并不用写代码，我会提供给你《用户自己写的代码》和《代码的执行结果》的信息。你只需要读代码和代码的结果就可以。
    
    ###
    用户写的代码：
    {code}
    
    ###
    代码的执行结果：
    {code_result}
    
    ###
    回复语气：
    请结合用户的提问，和代码运行的结果，以自然连贯的语气回答用户的问题。
    请不要再回复中提及代码相关的内容。
    
    ###
    用户输入：
    {query}
"""

prompt = PromptTemplate(
    template=template,
    input_variables=["code", "code_result", "query"]
)

chain = prompt | llm | StrOutputParser()


def python_tool(query: str):
    code = code_chain.invoke(query)
    color_print("===Python代码===", CODE_COLOR)
    color_print(code, CODE_COLOR)
    code_result = tool.invoke(code)
    color_print("===运行结果===", CODE_COLOR)
    color_print(str(code_result), CODE_COLOR)
    return chain.invoke({"code": code, "code_result": code_result, "query": query})


if __name__ == '__main__':
    result = python_tool("中原银行的负债率为10%，请问是否超过了20%")
    print(result)
