from digital_analyst.agent.research_agent import agent_executor

background_prompt = """
###
任务：
你的任务是“判断中原银行是否股东分散”。
参考我给你的<分析步骤>来解决问题。
你可以通过CompanyInfo工具获得中原银行的股东信息，还可以调用Calculator工具当你需要计算的时候。
请用中文思考和回答。

###
分析步骤：
请你按照以下步骤，将问题拆解为子问题，通过逐步解答子问题，最终找到问题的答案。
你可以按照以下步骤，将问题拆解为若干子问题，通过逐步解答子问题，最终找到问题的答案。
1.首先列出中原银行的前十大股东的股东名称，股东性质和持股比例。
2.获取第一大股东的持股比例
3.计算前十大股东的持股比例
4.判断第一大股东的持股比例是否小于10%
5.判断前十大股东的持股比例是否小于40%
6.如果第一大股东持股比例小于10%或者前十大股东持股比例小于40%，则认为股东分散。


###        
输出：
请回答中原银行是否股东分散，同时列出你回答问题的依据。
列出述子问题问题的答案。
"""

if __name__ == '__main__':

    chat_history = []

    response = agent_executor.invoke({"input": background_prompt, "chat_history": chat_history})['output']
    print(response)