from native.rag.embedding_util import get_embeddings
from native.rag.pdf_utils import extract_text_from_pdf, split_text
from prompt import build_prompt, prompt_template
from vector_db import MyVectorDBConnector
from native.llm import get_completion


class RagBot:
    def __init__(self, vector_db, llm_api, n_results=2):
        self.vector_db = vector_db
        self.llm_api = llm_api
        self.n_results = n_results

    def chat(self, user_query):
        # 1.检索
        search_results = self.vector_db.search(user_query, self.n_results)

        # 2.构建 Prompt
        prompt = build_prompt(prompt_template=prompt_template, info=search_results['documents'][0], query=user_query)

        # 3. 调用LLM
        response = self.llm_api(prompt)
        return response


if __name__ == '__main__':
    # 获取文本
    paragraphs = extract_text_from_pdf("../../static/llama2.pdf", min_line_length=10)
    chunks = split_text(paragraphs)
    # 存入向量数据库
    vector_db = MyVectorDBConnector("demo", get_embeddings)
    vector_db.add_documents(chunks)
    # 创建聊天机器人
    bot = RagBot(vector_db=vector_db, llm_api=get_completion)

    user_query = "llama2有对话版吗?"
    response = bot.chat(user_query=user_query)
    print(response)
