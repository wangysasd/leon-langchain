from leon_agent.bank_boys.component import pdf_tool, ag_agent
from leon_agent.data_model.leon_enum import SolutionType
from leon_agent.tools.writer_tool import get_report_summary
from langchain_community.chat_message_histories.in_memory import ChatMessageHistory

qa_str = ""


def qa_assistant(q, prompt=None, solution_type=SolutionType.RAG):
    global qa_str
    if prompt is None:
        prompt = q

    a = ""
    if solution_type == SolutionType.RAG:
        a = pdf_tool.invoke(prompt)
    if solution_type == SolutionType.Agent:
        a = ag_agent.run(prompt, ChatMessageHistory(), verbose=True)

    qa_str += "Q:" + q + "\n"
    qa_str += "A:" + a + "\n"
    qa_str += "\n"
    print(q)
    print(a)
    print("\n")


if __name__ == '__main__':
    bank_name = "中原银行股份有限公司"
    bank_class = "城市商业银行"

    # qa_str += "1.公司治理：\n"
    # qa_str += "1.1成立背景：\n"
    # qa_str += "\n"
    #
    # qa_assistant(f"{bank_name}是哪年成立的")
    # qa_assistant(f"{bank_name}的银行性质是什么")
    # qa_assistant(f"{bank_name}的注册资本有多少")
    #
    # qa_str += "1.2股东情况：\n"
    # qa_assistant(f"{bank_name}的大股东是谁。")
    #
    # dispersion_prompt = f"""
    # #### 任务
    # 判断{bank_name}是否股东分散，同时列出你回答问题的依据。
    #
    # #### 判定标准
    # -如果第一大股东持股小于10%或者前十大股东持股小于40%，则认为股东分散。
    #
    # #### 工具调用
    # -使用PdfQuery工具，来获取前十大股东的数据，包括股东性质和持股比例。以及第一大股东持股比例和前十大股东持股比例。
    # -使用MathTool工具，来判定持股比例是否超过某个值。
    # """
    # qa_assistant(q=f"{bank_name}是否有股东是否分散。",
    #              prompt=dispersion_prompt,
    #              solution_type=SolutionType.Agent)
    #
    # background_prompt = f"""
    # #### 任务
    # 判断{bank_name}是民营营背景强还是国有背景强(结果可以是都不强)，同时列出你回答问题的依据。
    #
    # #### 判定标准
    # -国有股东背景强的判定标准是前十大股东中国有股东持股比例超过30%。
    # -民营股东背景强的判定标准是第一大股东是民营股东，或者前十大股东中民营股东持股比例超过国有股东。
    #
    # #### 工具调用
    # -使用PdfQuery工具，来获取前十大股东的数据，包括股东性质和持股比例。
    # -使用MathTool工具，计算前十大股东中国有股东的持股比例。
    # -使用MathTool工具，计算前十大股东中民营股东的持股比例。
    # """
    # qa_assistant(q=f"{bank_name}是民营背景强还是国企背景强。",
    #              prompt=background_prompt,
    #              solution_type=SolutionType.Agent)
    #
    # qa_str += "1.3高管变动：\n"
    # qa_assistant(f"请列出{bank_name}的高管名单, 以及高管名单的近期的变动。")
    #
    # qa_str += "2.经营情况：\n"
    # qa_str += "2.1区域经济：\n"
    # qa_assistant(
    #     f"请对{bank_name}所处地区的区域生产总值、产业结构和增速进行描述，用数字说话，并在开头简单总结经济状况，说简洁一点。")

    # qa_str += "2.2资产端：\n"
    # qa_assistant(f"{bank_name}的总资产是多少。")
    # qa_assistant(f"简述{bank_name}的贷款情况，包括个人贷款和对公贷款。尽可能用数字说话。")

    # loan_prompt = f"""
    # ### 任务：
    # 判断{bank_name}是否零售基础好并说明依据。
    # 判断{bank_name}是否高度依赖对公贷款并说明依据。
    # 判断{bank_name}的对公贷款是否行业集中。
    # 判断{bank_name}的房地产敞口是否较大。
    #
    # ### 判定规则：
    # 如果个人贷款占比超过30%，则说明零售基础好。
    # 如果对公贷款占比高于70%，则说明高度依赖对公贷款。
    # 对公贷款的前五大行业的比例如果高于50%，就说明行业集中。
    # 如果涉及房地产的贷款比例超过15%，则说明地产敞口较大。
    #
    # ### 工具使用：
    # 使用PdfQuery工具，来获取个人贷款占比和对公贷款占比的数据。
    # 使用PdfQuery工具，来获取对公贷款的前五大行业。
    # 使用PdfQuery工具，来获取房地产行业的贷款占比。
    #
    # ### 返回要求：
    # 依次回答判定问题。
    # 若判定为是，先列出数据，再写判定结论。
    # 若判定为否，仅列出数据即可。
    # """
    #
    # qa_assistant(q=f"简述{bank_name}的贷款情况。",
    #              prompt=loan_prompt,
    #              solution_type=SolutionType.Agent)

    # finance_invest_prompt = f"""
    # ### 任务：
    # 获取{bank_name}的金融投资规模数据，若金融投资规模占比超过50%，则较为依赖金融投资。
    #
    # ### 工具使用：
    # 使用PdfQuery工具，来获取金融投资规模，债券投资占比，和利率债投资规模占比的数据。
    # """
    # qa_assistant(q=f"简述{bank_name}的金融投资情况。",
    #              prompt=finance_invest_prompt,
    #              solution_type=SolutionType.Agent)

    # bond_invest_prompt = f"""
    # ### 任务：
    # 获取{bank_name}的债券投资占投资资产总额的比例，若占比超过70%，则说明投资风格稳健。
    #
    # ### 工具使用：
    # 使用PdfQuery工具，来获取债券投资和投资资产总额的数据。
    # 使用MathTool工具，债券投资占投资资产总额的比例。
    # """
    # qa_assistant(q=f"简述{bank_name}的债券投资情况。",
    #              prompt=bond_invest_prompt,
    #              solution_type=SolutionType.Agent)
    #
    # qa_assistant(f"{bank_name}的信用债投资情况")

    # liability_prompt = f"""
    # ### 任务：
    # 获取并描述{bank_name}的[信托计划、资产管理计划及其他融资工具]在[投资资产总额]中的占比数据,以及这几年的变化。
    # 判断上述占比是否今年相较于去年相对下降了20%，若下降明显，则说明非标逐步压降。如果没有超过20%，则说明需要关注后续处置情况。
    #
    # ### 工具使用：
    # 使用PdfQuery工具，来获取[信托计划、资产管理计划及其他融资工具]在[投资资产总额]中的占比数据。
    # """
    #
    # qa_assistant(q=f"简述{bank_name}的非标压降情况。",
    #              prompt=liability_prompt,
    #              solution_type=SolutionType.Agent)

    # qa_str += "2.3负债端：\n"
    #
    # deposit_prompt = f"""
    # ### 任务：
    # 获取并描述{bank_name}的[零售存款（又叫储蓄存款）]在[存款总额]中的占比。
    # 判断上述占比是否超过了50%，如果超过则说明零售基础良好。
    #
    # ### 工具使用：
    # 使用PdfQuery工具，来获取[零售存款（又叫储蓄存款）]和[存款总额]中的占比数据。
    # """
    #
    # qa_assistant(q=f"简述{bank_name}的零售存款情况，是否零售基础良好。",
    #              prompt=deposit_prompt,
    #              solution_type=SolutionType.Agent)

    # interbank_liability_prompt = f"""
    # ### 任务：
    # 获取并描述{bank_name}的同业负债和占比情况。
    # 判断同业负债占比如果超过20%，则说明仰赖同业负债，如果不超过，则不用提。
    #
    # ### 工具使用：
    # 使用PdfQuery工具，来获取同业负债占比数据。
    # """
    #
    # qa_assistant(q=f"简述{bank_name}的同业负债占比数据。",
    #              prompt=interbank_liability_prompt,
    #              solution_type=SolutionType.Agent)

    # qa_assistant("请描述中原银行的存款规模、增速情况。可以从个人对公两个维度进行分析。用数字说话。")

    # qa_str += "3.财务风险：\n"
    # qa_str += "3.1资产质量：\n"
    #
    # asset_prompt = f"""
    # ### 任务：
    # 获取并描述{bank_name}的[关注类贷款占比]、[不良贷款率]、[逾期90天/不良]、[拨备覆盖率]、[非标投资/总资产]和[不良贷款净生成率]的数据。
    # 判断{bank_name}的[不良贷款率]和[拨备覆盖率]与{bank_class}相比如何。
    #
    # ### 工具使用：
    # 使用BankData工具，来{bank_name}的指标数据数据。
    # 使用IndustrialData工具，来获取{bank_class}的指标数据。
    #
    # 注意事项：
    # 指标单位为%，在答复描述中记得加上%。
    # """
    #
    # qa_assistant(q=f"列举{bank_name}的资产质量数据。",
    #              prompt=asset_prompt,
    #              solution_type=SolutionType.Agent)
    #
    # qa_assistant(f"请评价{bank_name}的资产质量如何")
    #
    # qa_str += "3.2盈利能力：\n"
    # asset_prompt = f"""
    # ### 任务：
    # 获取并描述{bank_name}的[资产收益率]、[净利润]、[净利润增长率]、[手续费及佣金收入占比]、[净息差]和[成本收入比]的数据。
    # 判断{bank_name}的[资产收益率]、[净利润]和[净息差]与{bank_class}相比如何。
    #
    # ### 工具使用：
    # 使用BankData工具，来{bank_name}的指标数据数据。
    # 使用IndustrialData工具，来获取{bank_class}的指标数据。
    #
    # 注意事项：
    # 除了净利润之外，其他指标的单位为%，在答复描述中记得加上%。
    # """
    #
    # qa_assistant(q=f"列举{bank_name}的盈利能力。",
    #              prompt=asset_prompt,
    #              solution_type=SolutionType.Agent)

    # qa_assistant(f"评价{bank_name}的盈利能力如何，有何财务风险。")

    # qa_str += "3.3资本情况：\n"
    # asset_prompt = f"""
    # ### 任务：
    # 获取并描述{bank_name}的[资本充足率]、[一级资本充足率]和[核心一级资本充足率]的数据。
    # 判断{bank_name}的[资本充足率]与{bank_class}的[资本充足率]相比如何。
    #
    # ### 工具使用：
    # 使用BankData工具，来{bank_name}的指标数据数据。
    # 使用IndustrialData工具，来获取{bank_class}的指标数据。
    #
    # 注意事项：
    # 指标单位为%，在答复描述中记得加上%。
    # """
    #
    # qa_assistant(q=f"列举{bank_name}的盈利能力数据。",
    #              prompt=asset_prompt,
    #              solution_type=SolutionType.Agent)
    #
    # qa_assistant(f"评价{bank_name}的资本充足情况")
    #
    #
    # print(qa_str)

    # with open("../bank_data/qa.txt", "a") as f:
    #     f.write(qa_str)

    final_report = get_report_summary()
    print(final_report)
    with open("../bank_data/final_report.txt", "w") as f:
        f.write(final_report)
