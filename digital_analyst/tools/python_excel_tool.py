# -*- coding: utf-8 -*-

import re

from langchain_community.utilities.python import PythonREPL
from langchain_core.output_parsers import BaseOutputParser, StrOutputParser
from langchain_core.prompts import PromptTemplate
from langchain_core.tools import StructuredTool

from digital_analyst.llm.llm import llm_gpt4, llm_gpt4o
from digital_analyst.tools.excel_tool import get_first_n_rows
from digital_analyst.util.CallbackHandlers import ColoredPrintHandler
from digital_analyst.util.PrintUtils import CODE_COLOR

llm = llm_gpt4o


class PythonCodeParser(BaseOutputParser):

    @staticmethod
    def __remove_marked_lines(input_str: str) -> str:
        """从OpenAI返回的文本中提取Python代码。"""
        lines = input_str.strip().split('\n')
        if lines and lines[0].strip().startswith('```'):
            del lines[0]
        if lines and lines[-1].strip().startswith('```'):
            del lines[-1]

        ans = '\n'.join(lines)
        return ans

    def parse(self, text: str) -> str:
        # 使用正则表达式找到所有的Python代码块
        python_code_blocks = re.findall(r'```python\n(.*?)\n```', text, re.DOTALL)
        # 从re返回结果提取出Python代码文本
        python_code = None
        if len(python_code_blocks) > 0:
            python_code = python_code_blocks[0]
            python_code = self.__remove_marked_lines(python_code)
        return python_code


class ExcelAnalyser:
    """
    通过程序脚本分析一个结构化文件(例如excel文件)的内容。
    输入中必须包括文件的完整路径和具体分析方式和分析依赖，阈值常量等。
    """

    def __init__(self, prompt_file="../prompt/tools/excel_analyser.txt", filename="../data/银行指标.xlsx", verbose=True, my_verbose=False):

        # with open(prompt_file, "r", encoding="utf-8") as f:
        #     template_content = f.read()
        # self.prompt = PromptTemplate.from_template(template_content)
        self.prompt = PromptTemplate.from_file(prompt_file)
        self.filename = filename
        self.verbose = verbose
        self.my_verbose = my_verbose
        self.verbose_handler = ColoredPrintHandler(CODE_COLOR)

    def analyse(self, query):
        """分析一个结构化文件(例如excel文件)的内容"""

        inspections = get_first_n_rows(self.filename, 100)

        code_parser = PythonCodeParser()

        chain = self.prompt | llm | StrOutputParser()

        response = ""

        if self.my_verbose:
            print("===========ExcelAnalyser发出去的prompt是这个样子的=================")
            print(self.prompt.format(
                query=query,
                filename=self.filename,
                inspections=inspections
            ))

        # ** {
        #     "query": query,
        #     "filename": filename,
        #     "inspections": inspections
        # }

        for c in chain.stream({
            "query": query,
            "filename": self.filename,
            "inspections": inspections
        },
                config={
                    "callbacks": [self.verbose_handler] if self.verbose else []
                }
        ):
            response += c

        code = code_parser.parse(response)

        if self.my_verbose:
            print("====用来运行的python代码======")
            print(code)

        if code:
            # ans = str(query) + "\n" + PythonREPL().run(code)
            # return ans
            return PythonREPL().run(code)
        else:
            return "没有找到可执行python文件"

    def as_tool(self):
        return StructuredTool.from_function(
            func=self.analyse,
            name="AnalyseExcel",
            description=self.__class__.__doc__.replace("\n", "")
        )


if __name__ == '__main__':

    excel_analyser = ExcelAnalyser(filename="../data/银行指标.xlsx")

    while True:
        query = input("请提问：")
        output = excel_analyser.analyse(query)
        print(output)