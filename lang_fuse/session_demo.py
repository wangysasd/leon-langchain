from langchain_core.messages import (
    AIMessage,
    HumanMessage,
    SystemMessage
)
from langfuse.callback import CallbackHandler
from basic_demo import model

messages = [
    SystemMessage(content="你是AGIClass的课程助理")
]

handler = CallbackHandler(
    user_id="wys",
    session_id="my_chat_session"
)

if __name__ == '__main__':
    while True:
        user_input = input("User:")
        if user_input.strip() == "":
            break
        messages.append(HumanMessage(content=user_input))
        response = model.invoke(messages, config={"callbacks": [handler]})
        print("AI:" + response.content)
        messages.append(response)
