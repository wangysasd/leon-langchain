import json
from native.llm import client

from native.function_call.util import print_json


def get_completion(messages, model="gpt-3.5-turbo-0125"):
    response = client.chat.completions.create(
        model=model,
        messages=messages,
        temperature=0.7,
        tools=[{
            "type": "function",
            "function": {
                "name": "sum",
                "description": "加法器，计算一组数的和",
                "parameters": {
                    "type": "object",
                    "properties": {
                        "numbers": {
                            "type": "array",
                            "items": {
                                "type": "number"
                            }
                        }
                    }
                }
            }
        }],
    )
    return response.choices[0].message


def parse_tool_result(response):
    if response.tool_calls is not None:
        # 是否要调用 sum
        tool_call = response.tool_calls[0]
        if tool_call.function.name == "sum":
            args = json.loads(tool_call.function.arguments)
            result = sum(args['numbers'])
            return tool_call.id, tool_call.function.name, result


def get_function_call_completion(prompt, verbose=False):
    messages = [
        {"role": "system", "content": "你是一个数学家"},
        {"role": "user", "content": prompt}
    ]

    # 第一轮原始回复
    response = get_completion(messages)
    messages.append(response)
    if verbose:
        print("=========GPT第一次回复=======")
        print_json(response)

    # 整理tool的结果数据
    tool_call_id, tool_call_function_name, result = parse_tool_result(response)
    messages.append(
        {
            "tool_call_id": tool_call_id,
            "role": "tool",
            "name": tool_call_function_name,
            "content": str(result)
        }
    )
    if verbose:
        print("==========整理tool数据=========")
        print(result)

    # 再次调用大模型获取最终结果
    return get_completion(messages).content


if __name__ == '__main__':
    prompt = "Tell me the sum of 1,2,3,4,5,6,7,8,9,10."
    # prompt = "桌上有 2 个苹果，四个桃子和 3 本书，一共有几个水果？"
    # prompt = "1+2+3...+99+100"
    # prompt = "1024 乘以 1024 是多少？"   # Tools 里没有定义乘法，会怎样？
    # prompt = "太阳从哪边升起？"           # 不需要算加法，会怎样？

    answer = get_function_call_completion(prompt)
    print("==========打印最终结果==========")
    print(answer)
