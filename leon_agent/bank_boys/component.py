from langchain_core.tools import StructuredTool

from leon_agent.agent.auto_gpt import AutoGPT
from leon_agent.llm import llm
from leon_agent.tools import PdfAnalyzer, finish_placeholder, math_tool, ExcelAnalyzer, python_agent
from leon_agent.tools.python_analyzer_tool import PythonAnalyzer

pdf_tool = PdfAnalyzer(pdf_file='../bank_data/中原银行股份有限公司2023年跟踪评级报告.pdf').as_query_tool()

python_analyzer_tool = PythonAnalyzer(verbose=True).as_tool(
    description='有关数学计算和逻辑推断的问题，write and execute python code to answer questions for user input')

bank_excel_query_tool = ExcelAnalyzer(excel_file='../bank_data/银行指标.xlsx', verbose=True).as_query_tool(
    name='BankData', description='获取银行的数据，调用这个方法。')

industrial_excel_query_tool = ExcelAnalyzer(excel_file='../bank_data/金融总局.xlsx', verbose=True).as_query_tool(
    name='IndustrialData', description='获取银行业整体的数据，调用这个方法。')

math_tool = StructuredTool.from_function(
    func=python_agent,
    name="MathTool",
    description="进行加减乘除运算的时候，使用这个工具",
)

ag_agent = AutoGPT(
    llm=llm,
    tools=[pdf_tool, python_analyzer_tool, math_tool,
           bank_excel_query_tool, industrial_excel_query_tool,
           finish_placeholder],
    work_dir="../data",
    main_prompt_file="../prompts/main/main.txt",
    max_thought_steps=20,
)


if __name__ == '__main__':
    response = industrial_excel_query_tool.invoke("城市商业银行的净息差")
    print(response)