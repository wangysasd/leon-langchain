from langchain.prompts import ChatPromptTemplate
from langchain.prompts.chat import SystemMessagePromptTemplate, HumanMessagePromptTemplate
from langchain_core.output_parsers import StrOutputParser
from langchain_core.prompts import PromptTemplate
from langchain_core.runnables import RunnablePassthrough

from leon_agent.llm import *
from leon_agent.utils.callback_handlers import ColoredPrintHandler
from leon_agent.utils.print_utils import THOUGHT_COLOR


def write(query: str, verbose=False):
    """按用户要求撰写文档"""
    template = ChatPromptTemplate.from_messages(
        [
            SystemMessagePromptTemplate.from_template(
                "你是专业的文档写手。你根据客户的要求，写一份文档。输出中文。"),
            HumanMessagePromptTemplate.from_template("{query}"),
        ]
    )

    chain = {"query": RunnablePassthrough()} | template | llm | StrOutputParser()

    return chain.invoke(query)


def get_report_summary():
    prompt = PromptTemplate.from_file("../prompts/tools/report_summary.txt")
    chain = prompt | llm_gpt4 | StrOutputParser()

    qa = PromptTemplate.from_file("../bank_data/qa_depreciate.txt")

    response = ""
    for c in chain.stream({
        "qa": qa
    },
            config={
                "callbacks": [ColoredPrintHandler(THOUGHT_COLOR)]
            }
    ):
        response += c
    return response


if __name__ == "__main__":
    print(write("写一封邮件给张三，内容是：你好，我是李四。"))
