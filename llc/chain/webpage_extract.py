from langchain.chains.llm import LLMChain
from langchain.chains.llm_requests import LLMRequestsChain
from langchain_core.prompts import PromptTemplate
from llc.model_io.models import llm


template = """在 >>> 和 <<< 之间是网页的返回的HTML内容。
网页是新浪财经A股上市公司的公司简介。
请抽取参数请求的信息。

>>> {requests_result} <<<
请使用如下的JSON格式返回数据
{{
  "company_name":"a",
  "company_english_name":"b",
  "issue_price":"c",
  "date_of_establishment":"d",
  "registered_capital":"e",
  "office_address":"f",
  "Company_profile":"g"

}}
Extracted:"""

prompt = PromptTemplate(
    input_variables=["requests_result"],
    template=template
)

llm_chain = LLMChain(llm=llm, prompt=prompt)
chain = LLMRequestsChain(llm_chain=llm_chain)

if __name__ == '__main__':
    inputs = {
      "url": "https://vip.stock.finance.sina.com.cn/corp/go.php/vCI_CorpInfo/stockid/600519.phtml"
    }
    response = chain(inputs)
    print(response['output'])

