from langchain_core.output_parsers import StrOutputParser
from langchain_core.prompts import PromptTemplate
from langchain_core.tools import StructuredTool
from langchain_experimental.utilities import PythonREPL

from leon_agent.llm import llm
from leon_agent.parser.python_code_parser import PythonCodeParser
from leon_agent.utils.callback_handlers import ColoredPrintHandler
from leon_agent.utils.print_utils import CODE_COLOR


class PythonAnalyzer:
    """
    通过程序脚本分析一个需要数学计算和逻辑推断的问题。
    """

    def __init__(self, prompt_file="../prompts/tools/python_analyser.txt", verbose=False):
        self.prompt = PromptTemplate.from_file(prompt_file)
        self.verbose = verbose
        self.verbose_handler = ColoredPrintHandler(CODE_COLOR)

    def analyse(self, query):
        """
        通过程序脚本分析推理一个需要数学计算和逻辑推断的问题。
        """
        chain = self.prompt | llm | StrOutputParser()
        response = ""
        for c in chain.stream({
            "query": query
        }, config={
            "callbacks": [
                self.verbose_handler
            ] if self.verbose else []
        }):
            response += c

        code_parser = PythonCodeParser()
        code = code_parser.parse(response)

        if code:
            # ans = query + "\n" + PythonREPL().run(code)
            ans = PythonREPL().run(code)
            return ans
        else:
            return "没有找到可执行的Python代码"

    def as_tool(self, name: str = None, description: str = None):
        return StructuredTool.from_function(
            func=self.analyse,
            name=name if name else "PythonAnalyser",
            description=description if description else self.__class__.__doc__.replace("\n", ""),
        )


if __name__ == '__main__':
    query = "笼子有鸡和兔子，鸡有两只脚一个头，兔子有四只脚一个头，一共有5个头，14只脚，请问有几个兔子，几只鸡。"
    response = PythonAnalyzer(verbose=True).analyse(query)
    print(response)
