from langchain_core.runnables import RunnablePassthrough
from digital_analyst.util.fileutil import *

llm = llm_gpt4

filename = "../data/中原银行股份有限公司2023年跟踪评级报告.pdf"
document = get_document(filename)

prompt = PromptTemplate.from_file("../prompt/tools/document_qa.txt").partial(**{"document": document})

runnable_chain = {"query": RunnablePassthrough()} | prompt | llm | StrOutputParser()

document_chain = prompt | llm | StrOutputParser()

if __name__ == '__main__':
    # output = chain.invoke("中原银行成立于哪一年")
    # print(output)
    while True:
        question = input("请提问:")
        output = runnable_chain.invoke(question)
        print(output)
