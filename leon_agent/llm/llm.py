from langchain_core.output_parsers import StrOutputParser

from config.config import CONFIG
from langchain_openai import ChatOpenAI, OpenAI
from langchain_openai import OpenAIEmbeddings

llm_gpt4 = ChatOpenAI(api_key=CONFIG.OPENAI_API_KEY, base_url=CONFIG.OPENAI_BASE_URL,
                      model_name="gpt-4-1106-preview", temperature=CONFIG.TEMPERATURE)
# model_kwargs = {
#     "seed": 42
# }

llm_gpt4o = ChatOpenAI(api_key=CONFIG.OPENAI_API_KEY, base_url=CONFIG.OPENAI_BASE_URL,
                       model_name="gpt-4o", temperature=CONFIG.TEMPERATURE)

llm_gpt4otool = ChatOpenAI(api_key=CONFIG.OPENAI_API_KEY, base_url=CONFIG.OPENAI_BASE_URL,
                           model_name="gpt-4o", temperature=CONFIG.TEMPERATURE)

llm_gpt3 = ChatOpenAI(api_key=CONFIG.OPENAI_API_KEY, base_url=CONFIG.OPENAI_BASE_URL,
                      model_name=CONFIG.MODEL_NAME, temperature=CONFIG.TEMPERATURE)

model = OpenAI(temperature=0, model_kwargs={"seed": 42}, )

embeddings = OpenAIEmbeddings(openai_api_key=CONFIG.OPENAI_API_KEY, openai_proxy=CONFIG.OPENAI_BASE_URL)

if __name__ == '__main__':
    # a = llm_gpt4.invoke("你好呀")

    chain = llm_gpt4

    a = chain.invoke("你好呀")

    print(a)
