from langchain_core.prompts import PromptTemplate
from langchain_core.tools import tool
from langchain_openai import ChatOpenAI
from openai import OpenAI

from digital_analyst.chain.basic_qa_chain import document_chain as holder_chain
from digital_analyst.chain.math_tool import compute
from digital_analyst.llm.llm import llm_gpt4

llm = llm_gpt4


@tool
def my_sum(num: list[float]) -> float:
    """求和运算"""
    return sum(num)


llm_with_tools = llm.bind_tools([my_sum])

template = """
            下面我将提供给你股东名称和股东持股比例的信息：
            股东 | 持股比例
            {shareholder_info}
            请计算出上述股东的持股比例之和
        """

holder_amount_prompt = PromptTemplate(
    template=template,
    input_variables=["shareholder_info"]
)

holder_amount_prompt = {"shareholder_info": holder_chain} | holder_amount_prompt | llm_with_tools | (
    lambda x: x.tool_calls[0]["args"]) | my_sum


if __name__ == '__main__':
    # a = holder_amount_prompt.invoke("中原银行前十大股东")
    # print(a)
    # output = ten_holder_chain.invoke("中原银行前十大股东的名称")
    # print(output)
    q = llm_with_tools.invoke("1+10=?")
    print(q)
