from util import *

assistant = client.beta.assistants.create(
    name="Demo Assistant",
    instructions="你是人工智能助手。你可以通过代码回答很多数学问题。",
    tools=[{"type": "code_interpreter"}],
    model="gpt-4-turbo"
)

thread = client.beta.threads.create()

if __name__ == '__main__':
    message = client.beta.threads.messages.create(
        thread_id=thread.id,
        role="user",
        content="用代码计算1234567的平方根"
    )

    print_result(assistant,thread)
