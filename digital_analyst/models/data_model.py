from typing import Optional
from langchain_core.pydantic_v1 import BaseModel, Field


class ConclusionResult(BaseModel):
    """大模型回答的结果"""

    conclusion: Optional[str] = Field(default=None, description="回答问题的结论。")

    reason: Optional[str] = Field(default=None, description="得到这个结论用到的原因")


class LeonMemory(BaseModel):
    title_one: Optional[str] = Field(default=None, description="一级标题")

    title_two: Optional[str] = Field(default=None, description="二级标题")

    question: Optional[str] = Field(default=None, description="问题")

    prompt: Optional[str] = Field(default=None, description="提示词")

    answer: Optional[str] = Field(default=None, description="回答")
