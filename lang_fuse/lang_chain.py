from langfuse.callback import CallbackHandler
from langchain.prompts import ChatPromptTemplate, HumanMessagePromptTemplate
from langchain_core.output_parsers import StrOutputParser
from langchain_openai import ChatOpenAI
from langchain_core.runnables import RunnablePassthrough
from config.config import CONFIG

handler = CallbackHandler(
    trace_name="SayHello",
    user_id="wys"
)

model = ChatOpenAI(api_key=CONFIG.OPENAI_API_KEY, base_url=CONFIG.OPENAI_BASE_URL,
                   model_name=CONFIG.MODEL_NAME, temperature=CONFIG.TEMPERATURE)

prompt = ChatPromptTemplate.from_messages([
    HumanMessagePromptTemplate.from_template("say hello to {input}")
])

parser = StrOutputParser()

chain = (
        {"input": RunnablePassthrough()}
        | prompt
        | model
        | parser
)

output = chain.invoke(input="leon wang", config={"callbacks": [handler]})

print(output)