你的任务是基于给定的上下文，基于用户输入回答问题。
如果在上下文中找不到答案，请回复“不知道”三个字，不带有任何标点符号。请不要编造答案。

上下文:
{document}

用户输入：
{query}