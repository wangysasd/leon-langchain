from langchain_community.document_loaders import PyPDFLoader
from langchain_core.prompts import ChatPromptTemplate
from langchain_text_splitters import RecursiveCharacterTextSplitter
from langchain_community.vectorstores import Chroma
from llc.model_io.models import embeddings
from langchain.schema.output_parser import StrOutputParser
from langchain.schema.runnable import RunnablePassthrough
from llc.model_io.models import llm

# Step 1: load
loader = PyPDFLoader(r".\data\llama2.pdf")
pages = loader.load_and_split()

# Step 2: 文本切割
text_splitter = RecursiveCharacterTextSplitter(
    chunk_size=200,
    chunk_overlap=100,
    length_function=len,
    add_start_index=True
)
split_documents = text_splitter.create_documents(
    [pages[2].page_content, pages[3].page_content]
)

# Step 3: 向量数据库
db = Chroma.from_documents(split_documents, embeddings)
retriever = db.as_retriever(search_kwargs={"k": 1})

# Step 4: Prompt & Chain
template = """Answer the question based only on the following context:
{context}

Question:  {question}
"""
prompt = ChatPromptTemplate.from_template(template)

rag_chain = (
        {"question": RunnablePassthrough(), "context": retriever}
        | prompt
        | llm
        | StrOutputParser()
)

if __name__ == '__main__':
    print(rag_chain.invoke("llama 2有多少参数？"))


