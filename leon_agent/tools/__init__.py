from .excel_analyzer_tool import ExcelAnalyzer
from .tools import *
from .file_qa_tool import *

# 自定义工具集
tools = [
    document_generation_tool,
    email_tool,
    excel_inspection_tool,
    directory_inspection_tool,
    finish_placeholder,
    ExcelAnalyzer(
        prompt_file="../prompts/tools/excel_analyser.txt",
        verbose=True
    ).as_analyze_tool(),
    PdfAnalyzer().as_analyze_tool(
        name="AskDocument",
        description="根据一个Word或PDF文档的内容，回答一个问题。考虑上下文信息，确保问题对相关概念的定义表述完整。")
]