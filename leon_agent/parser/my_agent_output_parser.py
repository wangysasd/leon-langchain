from typing import Union
from langchain.output_parsers import OutputFixingParser
from langchain_core.agents import AgentAction, AgentFinish
from langchain_core.output_parsers import PydanticOutputParser
from pydantic.v1 import BaseModel, Field
from langchain.agents import AgentOutputParser
from langchain_openai import ChatOpenAI


class MyAgentOutputParser(AgentOutputParser):
    """自定义parser，从思维链中取出最后的Y/N"""
    class AgentActionWrapper(BaseModel):
        tool: str = Field(..., title="The name of the Tool to execute.")
        tool_input: Union[str, dict] = Field(..., title="The input to pass in to the Tool.")

    __action_parser = OutputFixingParser.from_llm(
        parser=PydanticOutputParser(pydantic_object=AgentActionWrapper),
        llm=ChatOpenAI(
            model="gpt-3.5-turbo",
            temperature=0,
            model_kwargs={"seed": 42}
        )
    )

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def parse(self, text: str) -> Union[AgentAction, AgentFinish]:
        action = self.__action_parser.parse(text)
        if action.tool == "FINISH":
            return AgentFinish(log=text, return_values={
                "output": list(action.tool_input.values())[0]
                if isinstance(action.tool_input, dict)
                else action.tool_input
            })

        return AgentAction(
            tool=action.tool,
            tool_input=action.tool_input,
            log=text
        )

    def get_format_instructions(self) -> str:
        return self.__action_parser.get_format_instructions()