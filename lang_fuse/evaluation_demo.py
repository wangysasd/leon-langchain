import uuid

from langchain.prompts import PromptTemplate
from langchain_openai import ChatOpenAI
from langchain_core.output_parsers import StrOutputParser
from langfuse import Langfuse
from concurrent.futures import ThreadPoolExecutor
from langfuse.callback import CallbackHandler
from config.config import CONFIG


def simple_evaluation(output, expected_output):
    return output == expected_output


need_answer = PromptTemplate.from_template("""
*********
你是AIGC课程的助教，你的工作是从学员的课堂交流中选择出需要老师回答的问题，加以整理以交给老师回答。

课程内容:
{outlines}
*********
学员输入:
{user_input}
*********
如果这是一个需要老师答疑的问题，回复Y，否则回复N。
只回复Y或N，不要回复其他内容。""")

model = ChatOpenAI(api_key=CONFIG.OPENAI_API_KEY, base_url=CONFIG.OPENAI_BASE_URL,
                   model_name=CONFIG.MODEL_NAME, temperature=CONFIG.TEMPERATURE)
parser = StrOutputParser()
chain_v1 = need_answer | model | parser
langfuse = Langfuse()


def run_evaluation(chain, dataset_name, run_name):
    dataset = langfuse.get_dataset(dataset_name)

    def process_item(item):
        # handler = item.get_langchain_handler(run_name=run_name)
        handler = CallbackHandler(
            trace_name="SayHello",
            user_id="wys"
        )

        # Assuming chain.invoke is a synchronous function
        output = chain.invoke(item.input, config={"callbacks": [handler]})

        handler.root_span.score(
            name="accuracy",
            value=simple_evaluation(output, item.expected_output)
        )
        print('.', end='', flush=True)

    for item in dataset.items:
        process_item(item)


if __name__ == '__main__':
    run_evaluation(chain_v1, "assistant-data", "v1-" + str(uuid.uuid4())[:8])

    # with ThreadPoolExecutor(max_workers=4) as executor:
    # executor.map(process_item, dataset.items)
