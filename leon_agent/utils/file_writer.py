class FileWriter:
    def __init__(self, filename):
        """
        初始化 FileWriter 实例。

        :param filename: 要写入的文件名。
        """
        self.filename = filename

    def write_content(self, content):
        """
        将内容写入文件。

        :param content: 要写入文件的内容。
        """
        try:
            with open(self.filename, "w", encoding="utf-8") as file:
                file.write(content)
            print(f"内容已成功写入 {self.filename}")
        except Exception as e:
            print(f"写入文件时出错: {e}")