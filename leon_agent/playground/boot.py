from langchain.agents import AgentExecutor
from langchain_core.runnables.history import RunnableWithMessageHistory

from leon_agent.agent.auto_gpt import AutoGPT
from langchain_community.chat_message_histories.in_memory import ChatMessageHistory

from leon_agent.utils.callback_handlers import ColoredPrintHandler
from leon_agent.utils.print_utils import THOUGHT_COLOR


def launch_agent(agent: AutoGPT):
    human_icon = "\U0001F468"
    ai_icon = "\U0001F916"
    chat_history = ChatMessageHistory()

    while True:
        task = input(f"{ai_icon}：有什么可以帮您？\n{human_icon}：")
        if task.strip().lower() == "quit":
            break
        reply = agent.run(task, chat_history, verbose=True)
        print(f"{ai_icon}：{reply}\n")


def run_agent(agent, tools):
    human_icon = "\U0001F468"
    ai_icon = "\U0001F916"

    message_history = ChatMessageHistory()
    callback_handlers = [ColoredPrintHandler(color=THOUGHT_COLOR)]

    while True:
        task = input(f"{ai_icon}：有什么可以帮您？\n{human_icon}：")
        if task.strip().lower() == "quit":
            break
        agent_executor = AgentExecutor(
            agent=agent,
            tools=tools,
            handle_parsing_errors=True  # todo
        )

        # todo
        agent_with_chat_history = RunnableWithMessageHistory(
            agent_executor,
            lambda session_id: message_history,
            input_messages_key="input",
            history_messages_key="chat_history"
        )

        reply = ""
        chain_input = {"input": task}
        chain_config = {"configurable": {"session_id": "<foo>"},
                        "callbacks": callback_handlers}

        for s in agent_with_chat_history.stream(
                chain_input, chain_config
        ):
            if "output" in s:
                reply = s["output"]

        print(f"{ai_icon}:{reply}\n")
