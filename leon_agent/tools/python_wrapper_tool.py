from langchain_core.output_parsers import StrOutputParser
from langchain_core.output_parsers.openai_tools import JsonOutputKeyToolsParser
from langchain_core.prompts import PromptTemplate
from langchain_experimental.tools import PythonAstREPLTool

from leon_agent.llm import llm_tool, llm
from leon_agent.utils.print_utils import color_print, CODE_COLOR

tool = PythonAstREPLTool()
parser = JsonOutputKeyToolsParser(key_name=tool.name)
llm_with_tools = llm_tool.bind_tools([tool], tool_choice=tool.name)
code_chain = llm_with_tools | parser | (lambda x: x[0]['query'])

prompt = PromptTemplate.from_file("../prompts/tools/python_wrapper.txt")
chain = prompt | llm | StrOutputParser()


def python_wrapper(query: str, verbose: bool = True):
    code = code_chain.invoke(query)
    code_result = tool.invoke(code)
    if verbose:
        color_print("===Python代码===", CODE_COLOR)
        color_print(code, CODE_COLOR)
        color_print("===运行结果===", CODE_COLOR)
        color_print(str(code_result), CODE_COLOR)
    return chain.invoke({"code": code, "code_result": code_result, "query": query})


if __name__ == '__main__':

    query = "笼子有鸡和兔子，鸡有两只脚一个头，兔子有四只脚一个头，一共有5个头，14只脚，请问有几个兔子，几只鸡。"
    # response = code_chain.invoke("1+2=?")
    # print(response)
    # result = (llm_with_tools | parser).invoke(query)
    result = python_wrapper(query)
    print(result)
