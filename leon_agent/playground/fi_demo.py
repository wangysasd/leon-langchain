from langchain.agents import create_react_agent
from langchain_core.prompts import PromptTemplate

from leon_agent.agent.auto_gpt import AutoGPT
from leon_agent.llm import llm
from leon_agent.parser.my_agent_output_parser import MyAgentOutputParser
from leon_agent.playground.boot import launch_agent, run_agent
from leon_agent.tools import tools


def main():
    # 定义智能体
    agent = AutoGPT(
        llm=llm,
        tools=tools,
        work_dir="../fi_data",
        main_prompt_file="../prompts/main/main.txt",
        max_thought_steps=20,
    )

    # 运行智能体
    launch_agent(agent)


def main_lc():
    parser = MyAgentOutputParser()
    prompt = PromptTemplate.from_file("../prompts/main/main.txt").partial(
        work_dir="../fi_data", format_instructions=parser.get_format_instructions())

    agent = create_react_agent(
        llm=llm,
        tools=tools,
        prompt=prompt,
        output_parser=parser
    )

    run_agent(agent, tools)


if __name__ == "__main__":
    print("---------数字化实习生---------")
    main()
