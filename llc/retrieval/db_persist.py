from langchain_community.vectorstores import Chroma
from llc.model_io.models import embeddings
from langchain_community.document_loaders import PyPDFLoader

loader = PyPDFLoader(r".\data\llama2.pdf")
documents = loader.load_and_split()

# 持久化数据
docsearch = Chroma.from_documents(documents, embeddings, persist_directory="D:/vector_store")
docsearch.persist()

# 加载数据
docsearch = Chroma(persist_directory="D:/vector_store", embedding_function=embeddings)