import re

from langchain.tools import StructuredTool
from langchain_core.output_parsers import BaseOutputParser, StrOutputParser
from langchain_core.prompts import PromptTemplate

from leon_agent.llm import llm
from leon_agent.parser.python_code_parser import PythonCodeParser
from leon_agent.tools.excel_tool import get_first_n_rows, get_column_names
from leon_agent.utils.callback_handlers import ColoredPrintHandler
from leon_agent.utils.print_utils import CODE_COLOR
from langchain_experimental.utilities import PythonREPL


class ExcelAnalyzer:
    """
    通过程序脚本分析一个结构化文件（例如excel文件）的内容。
    输人中必须包含文件的完整路径和具体分析方式和分析依据，阈值常量等。
    """

    def __init__(self, prompt_file="../prompts/tools/excel_analyser.txt", excel_file=None, verbose=False):
        self.prompt = PromptTemplate.from_file(prompt_file)
        self.verbose = verbose
        self.verbose_handler = ColoredPrintHandler(CODE_COLOR)
        self.excel_file = excel_file

    def query(self, query: str) -> str:
        """分析一个结构化文件（例如excel文件）的内容。"""
        # columns = get_column_names(filename)
        inspections = get_first_n_rows(self.excel_file, 20)

        code_parser = PythonCodeParser()
        chain = self.prompt | llm | StrOutputParser()

        response = ""

        for c in chain.stream({
            "query": query,
            "filename": self.excel_file,
            "inspections": inspections
        }, config={
            "callbacks": [
                self.verbose_handler
            ] if self.verbose else []
        }):
            response += c

        code = code_parser.parse(response)
        if code:
            ans = PythonREPL().run(code)
            return ans
        else:
            return "没有找到可执行的Python代码"

    def analyze(self, query: str, filename: str) -> str:
        self.excel_file = filename
        return self.query(query)

    def as_analyze_tool(self, name: str = None, description: str = None):
        return StructuredTool.from_function(
            func=self.analyze,
            name=name if name else "ExcelAnalyzer",
            description=description if description else self.__class__.__doc__.replace("\n", ""),
        )

    def as_query_tool(self, name: str = None, description: str = None):
        return StructuredTool.from_function(
            func=self.query,
            name=name if name else "ExcelQuery",
            description=description if description else self.__class__.__doc__.replace("\n", ""),
        )


if __name__ == "__main__":
    print(ExcelAnalyzer(verbose=True).analyze(
        query="8月销售额",
        filename="../data/2023年8月-9月销售记录.xlsx"
    ))

#     code = '''
# import pandas as pd
#
# # 读取Excel文件
# file_path = '../data/2023年8月-9月销售记录.xlsx'
# df = pd.read_excel(file_path, sheet_name='2023年8月-9月销售记录')
#
# # 将销售日期列转换为datetime类型
# df['销售日期'] = pd.to_datetime(df['销售日期'])
#
# # 筛选出8月份的数据
# august_data = df[df['销售日期'].dt.month == 8]
#
# # 计算8月份的销售额
# august_data['销售额'] = august_data['单价(元)'] * august_data['销售量']
# total_sales_august = august_data['销售额'].sum()
#
# # 输出8月份的销售额
# print(f"2023年8月的销售额为: {total_sales_august}元")'''
#
#     response = PythonREPL().run(code)
#     print(response)