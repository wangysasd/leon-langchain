from util import *

# 上传文件到 OpenAI
file = client.files.create(
    file=open("mydata.csv", "rb"),
    purpose='assistants'
)

# 创建 assistant
# noinspection PyPackageRequirements
assistant = client.beta.assistants.create(
    name="CodeInterpreterWithFileDemo",
    instructions="你是数据分析师，按要求分析数据。",
    model="gpt-4-turbo",
    tools=[{"type": "code_interpreter"}],
    tool_resources={
        "code_interpreter": {
            "file_ids": [file.id]  # 为 code_interpreter 关联文件
        }
    }
)

thread = client.beta.threads.create()

if __name__ == '__main__':
    # 添加新一轮的 user message
    message = client.beta.threads.messages.create(
        thread_id=thread.id,
        role="user",
        content="统计总销售额",
    )

    print_result(assistant, thread)

    # with client.beta.threads.runs.stream(
    #     thread_id=thread.id,
    #     assistant_id=assistant.id,
    #     event_handler=EventHandler()
    # ) as stream:
    #     stream.until_done()
