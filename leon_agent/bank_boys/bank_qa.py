from leon_agent.bank_boys.component import ag_agent, bank_excel_query_tool, pdf_tool
from langchain_community.chat_message_histories.in_memory import ChatMessageHistory

if __name__ == '__main__':

    chat_history = ChatMessageHistory()

    while True:
        query = input("请提问:\n")
        if query.strip().lower() == "quit":
            break

        # 用QA工具
        response = pdf_tool.invoke(query)

        # 用EXCEL工具
        # response = bank_excel_query_tool.invoke(query)

        # 用ag-agent
        # response = ag_agent.run(query, ChatMessageHistory(), verbose=True)
        print(response)
        print("\n")

# QA
# 中原银行是哪年成立的
# 中原银行的银行性质是什么
# 中原银行的注册资本有多少



# 如果第一大股东是民企或前十大股东民企持股超过国企，那么民营背景强。请判断中原银行是否民营背景强。
# 如果前十大股东中的国有股东超过30%，那么国有背景强。请判断中原银行是否国有背景强并给出依据。