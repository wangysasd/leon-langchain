from typing import List
from langchain.schema import Document
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.vectorstores import Chroma
from langchain_community.document_loaders import PyPDFLoader
from langchain_community.document_loaders import UnstructuredWordDocumentLoader
from langchain.chains import RetrievalQA
from langchain_core.output_parsers import StrOutputParser
from langchain_core.prompts import PromptTemplate
from langchain_core.tools import StructuredTool

from leon_agent.llm import *
from leon_agent.llm.llm import embeddings
from leon_agent.utils.callback_handlers import ColoredPrintHandler
from leon_agent.utils.print_utils import CODE_COLOR


class FileLoadFactory:
    @staticmethod
    def get_loader(filename: str):
        ext = get_file_extension(filename)
        if ext == "pdf":
            return PyPDFLoader(filename)
        elif ext == "docx" or ext == "doc":
            return UnstructuredWordDocumentLoader(filename)
        else:
            raise NotImplementedError(f"File extension {ext} not supported.")


def get_file_extension(filename: str) -> str:
    return filename.split(".")[-1]


def load_docs(filename: str) -> List[Document]:
    file_loader = FileLoadFactory.get_loader(filename)
    pages = file_loader.load_and_split()
    return pages


def load_full_text(filename: str):
    file_loader = FileLoadFactory.get_loader(filename)
    raw_docs = file_loader.load()
    if len(raw_docs) == 0:
        return "抱歉，文档为空"

    document = ""
    for doc in raw_docs:
        document += str(doc.page_content)
    return document


def ask_document(
        filename: str,
        query: str,
) -> str:
    """根据一个PDF文档的内容，回答一个问题"""

    raw_docs = load_docs(filename)
    if len(raw_docs) == 0:
        return "抱歉，文档内容为空"
    text_splitter = RecursiveCharacterTextSplitter(
        chunk_size=200,
        chunk_overlap=100,
        length_function=len,
        add_start_index=True,
    )
    documents = text_splitter.split_documents(raw_docs)
    if documents is None or len(documents) == 0:
        return "无法读取文档内容"
    db = Chroma.from_documents(documents, embeddings)
    qa_chain = RetrievalQA.from_chain_type(
        llm=llm,  # 语言模型
        chain_type="stuff",  # prompt的组织方式，后面细讲
        retriever=db.as_retriever()  # 检索器
    )
    response = qa_chain.invoke(query + "(请用中文回答)")
    return response


class PdfAnalyzer:
    """
    根据一个Word或PDF文档的内容，回答一个问题。考虑上下文信息，确保问题对相关概念的定义表述完整。
    """

    def __init__(self, prompt_file="../prompts/tools/pdf_analyzer.txt", pdf_file=None, verbose=False):
        self.prompt = PromptTemplate.from_file(prompt_file)
        self.verbose = verbose
        self.verbose_handler = ColoredPrintHandler(CODE_COLOR)
        self.pdf_file = pdf_file
        if pdf_file:
            document = load_full_text(pdf_file)
            self.prompt = self.prompt.partial(**{"document": document})

    def query(self, query: str) -> str:
        """
        根据用户问题在给定的PDF文件中查询答案并回复。
        """

        chain = self.prompt | llm_gpt4 | StrOutputParser()

        response = ""
        for c in chain.stream({
            "query": query,
        }, config={
            "callbacks": [
                self.verbose_handler
            ] if self.verbose else []
        }):
            response += c
        return response

    def analyze(self, query: str, filename: str):
        document = load_full_text(filename) if filename else ""

        chain = self.prompt | llm | StrOutputParser()
        response = ""
        for c in chain.stream({
            "query": query,
            "document": document
        }, config={
            "callbacks": [
                self.verbose_handler
            ] if self.verbose else []
        }):
            response += c
        return response

    def as_query_tool(self, name: str = None, description: str = None):
        return StructuredTool.from_function(
            func=self.query,
            name=name if name else "PdfQuery",
            description=description if description else self.__class__.__doc__.replace("\n", ""),
        )

    def as_analyze_tool(self, name: str = None, description: str = None):
        return StructuredTool.from_function(
            func=self.analyze,
            name=name if name else "PdfAnalyzer",
            description=description if description else self.__class__.__doc__.replace("\n", ""),
        )


if __name__ == "__main__":
    ##TODO 换文件改下面路径
    filename = "../bank_data/成都银行股份有限公司2024年度跟踪评级报告.pdf"
    tool = PdfAnalyzer(pdf_file=filename, verbose=True).as_query_tool()

    while True:
        query = input("请输入问题：")
        response = tool.invoke({"query": query})
        print("回答：")
        print(response)
        print("----------------------")
