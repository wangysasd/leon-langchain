from langchain.schema import (AIMessage, HumanMessage, SystemMessage)
from langchain.prompts import PromptTemplate
from langchain.prompts import (ChatPromptTemplate, HumanMessagePromptTemplate, SystemMessagePromptTemplate,
                               MessagesPlaceholder)
from models import llm

# Demo1: 对话
messages = [SystemMessage(content='你是西瓜的爸爸，你喜欢陪西瓜玩'),
            HumanMessage(content='你好'),
            AIMessage(content='你好！有什么可以帮助你的吗？'),
            HumanMessage(content='我想和西瓜玩游戏，有什么推荐的')]

# Demo2: 模板
template = PromptTemplate.from_template("给我讲个关于{subject}的笑话")

# Demo3: 对话模板
message_template = ChatPromptTemplate.from_messages(
    [
        SystemMessagePromptTemplate.from_template("你是{product}的客服助手。你的名字叫{name}"),
        HumanMessagePromptTemplate.from_template("{query}")
    ]
)

# Demo4 :对话占位
human_message = HumanMessage(content='Who is Elon Musk')
ai_message = AIMessage(content='Elon Musk is a billionare entrepreneur, inventor , and industrial designer')
human_message_template = HumanMessagePromptTemplate.from_template("Translate your answer to {language}.")
chat_prompt = ChatPromptTemplate.from_messages(
    [MessagesPlaceholder(variable_name="conversation"), human_message_template]
)
placeholder_message = chat_prompt.format_prompt(conversation=[human_message, ai_message], language="日文")

if __name__ == "__main__":
    # Demo1:
    # print("Demo1:")
    # demo1_response = llm.invoke(messages)
    # print(demo1_response.content)

    # Demo2:
    # print("Demo2:")
    # print(template.format(subject='小明'))

    # Demo3:
    # print("Demo3:")
    # demo3_prompt = message_template.format_messages(
    #     product="PAI",
    #     name="智能小助手",
    #     query="你是谁"
    # )
    # print(llm.invoke(demo3_prompt).content)

    # Demo4:
    # print(placeholder_message.to_messages())
    print(llm.invoke(placeholder_message).content)
