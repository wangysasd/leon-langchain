from langchain.chains import LLMChain
from langchain.prompts import PromptTemplate
from langchain.chains import SimpleSequentialChain
from llc.model_io.models import llm

# location链条
template1 = """Your job is to come up with a classic dish from the area that the users suggests.
% USER LOCATION
{user_location}

YOUR RESPONSE:
"""
prompt_template1 = PromptTemplate(input_variables=["user_location"], template=template1)
location_chain = LLMChain(llm=llm, prompt=prompt_template1, verbose=True)

# meal链条
template2 = """Given a meal, give a short and simple recipe on how to make that dish at home.
% MEAL
{user_meal}

YOUR RESPONSE:
"""
prompt_template2 = PromptTemplate(input_variables=["user_meal"], template=template2)
meal_chain = LLMChain(llm=llm, prompt=prompt_template2, verbose=True)

overall_chain = SimpleSequentialChain(chains=[location_chain, meal_chain], verbose=True)

if __name__ == '__main__':
    overall_chain.invoke('Rome')
