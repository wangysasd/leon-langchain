from typing import List, Tuple

from langchain.memory import ConversationTokenBufferMemory
from langchain.memory.chat_memory import BaseChatMemory
from langchain.output_parsers import OutputFixingParser
from langchain_core.language_models import BaseChatModel
from langchain_core.output_parsers import PydanticOutputParser, StrOutputParser
from langchain_core.prompts import HumanMessagePromptTemplate, ChatPromptTemplate, MessagesPlaceholder
from langchain_core.tools import BaseTool, render_text_description
from langchain_community.chat_message_histories.in_memory import ChatMessageHistory
from pydantic import ValidationError

from leon_agent.data_model.action import Action
from leon_agent.utils.callback_handlers import *


class AutoGPT:

    @staticmethod
    def __format_short_term_memory(memory: BaseChatMemory) -> str:
        messages = memory.chat_memory.messages
        string_messages = [messages[i].content for i in range(1, len(messages))]
        return "\n".join(string_messages)

    def __init__(self,
                 llm: BaseChatModel,
                 tools: List[BaseTool],
                 work_dir: str,
                 main_prompt_file: str,
                 max_thought_steps: Optional[int] = 10):
        self.llm = llm
        self.tools = tools
        self.work_dir = work_dir
        self.main_prompt_file = main_prompt_file
        self.max_thought_steps = max_thought_steps
        self.verbose_handler = ColoredPrintHandler(color=THOUGHT_COLOR)

        # OutputFixingParser: 如果输出格式不正确，尝试修复。
        self.output_parser = PydanticOutputParser(pydantic_object=Action)
        self.robust_parser = OutputFixingParser.from_llm(
            parser=self.output_parser,
            llm=self.llm
        )

        self.__init_prompt_template()
        self.__init_chains()

    def __init_prompt_template(self):

        with open(self.main_prompt_file, 'r', encoding='utf-8') as f:
            self.prompt = ChatPromptTemplate.from_messages(
                [MessagesPlaceholder(variable_name="chat_history"),
                 HumanMessagePromptTemplate.from_template(f.read())]
            ).partial(
                work_dir=self.work_dir,
                tools=render_text_description(self.tools),
                tool_names=','.join([tool.name for tool in self.tools]),
                format_instructions=self.output_parser.get_format_instructions(),
            )

    def __init_chains(self):
        self.main_chain = (self.prompt | self.llm | StrOutputParser())

    def __step(self, task, short_term_memory, chat_history, verbose=False) -> Tuple[Action, str]:
        """执行一步思考"""

        chain_input = {
            "input": task,
            "agent_scratchpad": self.__format_short_term_memory(short_term_memory),
            "chat_history": chat_history.messages,
        }

        chain_config = {
            "callbacks": [self.verbose_handler] if verbose else []
        }

        response = ""
        for s in self.main_chain.stream(chain_input, chain_config):
            response += s

        action = self.robust_parser.parse(response)
        return action, response

    def __find_tool(self, tool_name: str) -> Optional[BaseTool]:
        for tool in self.tools:
            if tool.name == tool_name:
                return tool
        return None

    def __exec_action(self, action: Action) -> str:

        tool = self.__find_tool(action.name)
        if tool is None:
            observation = (
                f"Error: 找不到工具或指令 '{action.name}'",
                "请从提供的工具/指令列表中选择，请确保按对顶格式输出"
            )
        else:
            try:
                observation = tool.run(action.args)
            except ValidationError as e:
                observation = (
                    f"Validation Error in args: {str(e)}, args: {action.args}"
                )
            except Exception as e:
                observation = f"Error: {str(e)}, {type(e).__name__}, args:{action.args}"
        return observation

    def run(self,
            task: str,
            chat_history: ChatMessageHistory,
            verbose=False) -> str:

        short_term_memory = ConversationTokenBufferMemory(
            llm=self.llm
        )
        thought_step_count = 0
        reply = ""

        while thought_step_count < self.max_thought_steps:
            if verbose:
                ColoredPrintHandler.on_thought_start(thought_step_count)

            action, response = self.__step(task=task, short_term_memory=short_term_memory, chat_history=chat_history,
                                           verbose=verbose)

            if action.name == "FINISH":
                reply = self.__exec_action(action)
                break

            observation = self.__exec_action(action)

            if verbose:
                self.verbose_handler.on_tool_end(observation)

            short_term_memory.save_context({"input": response}, {"output": "\n返回结果\n" + str(observation)})
            thought_step_count += 1

        if thought_step_count >= self.max_thought_steps:
            reply = "抱歉，我没能完成任务"

        chat_history.add_user_message(task)
        chat_history.add_ai_message(reply)
        return reply
