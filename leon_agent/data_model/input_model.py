from pydantic.v1 import BaseModel, Field


class FinishInput(BaseModel):
    the_final_answer: str = Field(description="最终结果")
