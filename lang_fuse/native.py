from langfuse.openai import openai
from langfuse import Langfuse
from config.config import CONFIG

langfuse = Langfuse(secret_key=CONFIG.LANGFUSE_SECRET_KEY,
                    public_key=CONFIG.LANGFUSE_PUBLIC_KEY,
                    host=CONFIG.LANGFUSE_HOST)

trace = langfuse.trace(
    name="hello-world",
    user_id="wys",
    release="v0.0.1")

# country = "Bulgaria"
#
# capital = openai.chat.completions.create(
#     model="gpt-3.5-turbo",
#     messages=[
#         {"role": "system", "content": "What is the capital of the country?"},
#         {"role": "user", "content": country}],
#     name="get-capital",
#     trace_id=trace.trace_id
# ).choices[0].message.content
#
# print(capital)

completion = openai.chat.completions.create(
    name="hello-world",
    model="gpt-3.5-turbo",
    messages=[
        {"role": "user", "content": "对我说'Hello, World!'"}
    ],
    temperature=0,
    trace_id=trace.trace_id,
)

print(completion.choices[0].message.content)
