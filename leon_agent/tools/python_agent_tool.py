from langchain import hub
from langchain.agents import create_openai_functions_agent, AgentExecutor, create_react_agent
from langchain_core.prompts import PromptTemplate
from langchain_experimental.tools import PythonREPLTool

from leon_agent.llm import llm, llm_gpt4

instructions = """You are an agent designed to write and execute python code to answer questions.
You have access to a python REPL, which you can use to execute python code.
If you get an error, debug your code and try again.
Only use the output of your code to answer the question.
You might know the answer without running any code, but you should still run the code to get the answer.
If it does not seem like you can write code to answer the question, just return "I don't know" as the answer.
"""
# base_prompt = hub.pull("langchain-ai/react-agent-template")

base_prompt = PromptTemplate.from_file("../prompts/tools/python_agent_tool.txt")

prompt = base_prompt.partial(instructions=instructions, chat_history="")
tools = [PythonREPLTool()]

py_agent = create_react_agent(llm_gpt4, tools, prompt)
py_agent_executor = AgentExecutor(agent=py_agent, tools=tools, handle_parsing_errors=True, verbose=True)


def python_agent(query: str) -> str:
    return py_agent_executor.invoke({"input": query})['output']


if __name__ == '__main__':
    # # query = "What is the 10th fibonacci number?"
    query = "笼子有鸡和兔子，鸡有两只脚一个头，兔子有四只脚一个头，一共有5个头，14只脚，请问有几个兔子，几只鸡。"
    response = python_agent(query)
    print("------------")
    print(response)


