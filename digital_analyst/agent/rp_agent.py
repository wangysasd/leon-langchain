from langchain import hub
from langchain.agents import AgentExecutor, create_react_agent
from langchain.chains.llm_math.base import LLMMathChain
from langchain_core.messages import HumanMessage, AIMessage
from langchain_core.tools import Tool

from digital_analyst.tools.python_tool import python_tool
from digital_analyst.util.fileutil import get_answer_from_doc
from digital_analyst.llm.llm import llm_gpt4

llm = llm_gpt4

# 自定义工具字典
rp_tools = [
    Tool(
        name="PythonTool",
        description="当你需要数学运算、逻辑运算判断的时候，调用这个工具",
        func=python_tool
    ),

    Tool(
        name="CompanyInfo",
        description="当查询中原银行数据和信息的时候，调用这个方法",
        func=get_answer_from_doc,
    )
]

prompt = hub.pull("hwchase17/react-chat")
rp_agent = create_react_agent(llm, rp_tools, prompt)
rp_agent_executor = AgentExecutor(agent=rp_agent, tools=rp_tools, handle_parsing_errors=True, verbose=True)

if __name__ == '__main__':
    question = "中原银行的同业负债是规模和占比情况，如果同业负债占比超过20%（请用PythonTool来做判断），要点明其仰赖同业负债。"

    chat_history = []
    response = rp_agent_executor.invoke({"input": question, "chat_history": chat_history})['output']
    print(response)

    #
    # while True:
    #     question = input("请提问：")
    #     response = python_agent_executor.invoke({"input": question, "chat_history": chat_history})['output']
    #     print(response)
    #     chat_history.extend(HumanMessage(content=question))
    #     chat_history.extend(AIMessage(content=response))
