from native.llm import client


def get_completion(messages, model="gpt-3.5-turbo-0125"):
    response = client.chat.completions.create(
        model=model,
        messages=messages,
        temperature=0.7,
        tools=[{
            "type": "function",
            "function": {
                "name": "sum",
                "description": "加法器，计算一组数的和",
                "parameters": {
                    "type": "object",
                    "properties": {
                        "numbers": {
                            "type": "array",
                            "items": {
                                "type": "number"
                            }
                        }
                    }
                }
            }
        }],
        stream=True
    )
    return response


def get_completion_stream(prompt):
    message = [
        {"role": "system", "content": "你是一个小学数学老师，你要教学生加法"},
        {"role": "user", "content": prompt}
    ]

    response = get_completion(message)

    function_name, args, text = "", "", ""

    for msg in response:
        delta = msg.choices[0].delta
        if delta.tool_calls:
            if not function_name:
                function_name = delta.tool_calls[0].function.name
                print(function_name)
            args_delta = delta.tool_calls[0].function.arguments
            print(args_delta)
            args = args + args_delta
        elif delta.content:
            text_delta = delta.content
            print(text_delta)
            text = text + text_delta

    return function_name, args, text


if __name__ == '__main__':
    prompt = "1+2+3"
    function_name, args, text= get_completion_stream(prompt)
    print("==============finish===============")
    print(function_name)
    print(args)
    print(text)