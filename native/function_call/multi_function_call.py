import json

from native.llm import client
from native.function_call.util import print_json, get_location_coordinate, search_nearby_pois


def get_completion(messages, model="gpt-3.5-turbo"):
    response = client.chat.completions.create(
        model=model,
        messages=messages,
        temperature=0,
        seed=1024,  # 随机种子保持不变，temperature 和 prompt 不变的情况下，输出就会不变
        tool_choice="auto",  # 默认值，由 GPT 自主决定返回 function call 还是返回文字回复。也可以强制要求必须调用指定的函数，详见官方文档
        tools=[{
            "type": "function",
            "function": {
                "name": "get_location_coordinate",
                "description": "根据POI名称，获得POI的经纬度坐标",
                "parameters": {
                    "type": "object",
                    "properties": {
                        "location": {
                            "type": "string",
                            "description": "POI名称，必须是中文",
                        },
                        "city": {
                            "type": "string",
                            "description": "POI所在的城市名，必须是中文",
                        }
                    },
                    "required": ["location", "city"],
                }
            }
        },
            {
                "type": "function",
                "function": {
                    "name": "search_nearby_pois",
                    "description": "搜索给定坐标附近的poi",
                    "parameters": {
                        "type": "object",
                        "properties": {
                            "longitude": {
                                "type": "string",
                                "description": "中心点的经度",
                            },
                            "latitude": {
                                "type": "string",
                                "description": "中心点的纬度",
                            },
                            "keyword": {
                                "type": "string",
                                "description": "目标poi的关键字",
                            }
                        },
                        "required": ["longitude", "latitude", "keyword"],
                    }
                }
            }],
    )
    return response.choices[0].message


def get_multi_function_call_completion(prompt, verbose=False):
    global result
    messages = [
        {"role": "system", "content": "你是一个地图通，你可以找到任何地址"},
        {"role": "user", "content": prompt}
    ]

    response = get_completion(messages)
    messages.append(response)

    if verbose:
        print("=====GPT第一次回复======")
        print_json(response)

    while response.tool_calls is not None:

        # 支持一个返回多个函数调用请求
        for tool_call in response.tool_calls:
            args = json.loads(tool_call.function.arguments)
            if verbose:
                print("函数参数展开:")
                print_json(args)

            # 函数路由
            if tool_call.function.name == "get_location_coordinate":
                result = get_location_coordinate(**args)
            elif tool_call.function.name == "search_nearby_pois":
                result = search_nearby_pois(**args)

            messages.append({
                "tool_call_id": tool_call.id,
                "role": "tool",
                "name": tool_call.function.name,
                "content": str(result)
            })

        # 把tool的response都解析放到messages里了，最后再调用一次大模型
        response = get_completion(messages)
        messages.append(response)

    return response


if __name__ == '__main__':
    prompt = "我想在北京五道口附近喝咖啡，给我推荐几个"
    answer = get_multi_function_call_completion(prompt, True)
    print(answer.content)
